<?php

namespace App\Http\Controllers\Akademik;

use App\Http\Controllers\Core_Controller;
use App\Models\Akademik\Akademik as Akdemik_Model;
use Illuminate\Http\Request;

class Akademik extends Core_Controller
{

    public function __construct(Akdemik_Model $akademik)
    {
        parent::__construct();
        $this->akademik = $akademik;
    }

    public function index($type)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $menu = ['kelas', 'tahun_ajaran', 'pelajaran', 'jurusan'];
        if (in_array($type, $menu)) {
            $get_data = $this->akademik->get_akademik($type);
            return view('akademik/akademik', ['data' => $get_data, 'type' => $type]);
        } else {
            return abort(404);
        }
    }

    public function get_akademik($type)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $menu = ['kelas', 'tahun_ajaran', 'pelajaran', 'jurusan'];
        if (in_array($type, $menu)) {
            $get_data = json_encode($this->akademik->get_akademik($type));
            echo $get_data;
        } else {
            echo '';
        }
    }

    public function add(Request $request, $type)
    {
        $data = $request->only('input_akademik');
        $insert = $this->akademik->add($data, $type);
        echo $insert;
    }
    public function edit(Request $request, $type)
    {
        $data = $request->only('id', 'input_akademik');
        $insert = $this->akademik->edit($data, $type);
        echo $insert;
    }
    public function delete(Request $request, $type)
    {
        $data = $request->only('id');
        $insert = $this->akademik->delet($data, $type);
        echo $insert;
    }
}
