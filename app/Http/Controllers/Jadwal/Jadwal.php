<?php

namespace App\Http\Controllers\Jadwal;

use App\Http\Controllers\Core_Controller;
use App\Models\Jadwal\Jadwal as Jadwal_Model;
use Illuminate\Http\Request;

class Jadwal extends Core_Controller
{

    public function __construct(Jadwal_Model $jadwal)
    {
        parent::__construct();
        $this->jadwal = $jadwal;
    }

    public function index()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        return view('jadwal/index');
    }
}
