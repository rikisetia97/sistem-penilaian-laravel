<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Core_Controller;
use App\Models\Dashboard\Dashboard as Dashboard_Model;
use Illuminate\Support\Facades\Redirect;

class Dashboard extends Core_Controller
{

    public function __construct(Dashboard_Model $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    public function index()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $get_widget = $this->dashboard->get_widget();
        return view('dashboard', ['widget' => $get_widget]);
    }
}
