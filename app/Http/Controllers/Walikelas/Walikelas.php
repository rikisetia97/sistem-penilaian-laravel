<?php

namespace App\Http\Controllers\Walikelas;

use App\Http\Controllers\Core_Controller;
use App\Models\Walikelas\Walikelas as Walikelas_Model;
use Illuminate\Http\Request;

class Walikelas extends Core_Controller
{

    public function __construct(Walikelas_Model $walikelas)
    {
        parent::__construct();
        $this->walikelas = $walikelas;
    }

    public function index()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $kelas = $this->walikelas->get_all_data('kelas');
        $jurusan = $this->walikelas->get_all_data('jurusan');
        $guru = $this->walikelas->get_all_data('users');
        return view('walikelas/index', ['kelas' => $kelas, 'jurusan' => $jurusan, 'guru' => $guru]);
    }

    function get_walikelas()
    {
        $walikelas = $this->walikelas->get_walikelas();
        echo json_encode($walikelas);
    }


    public function insert_walikelas(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $response = $this->walikelas->insert_walikelas($data);
        echo $response;
    }
    public function update_walikelas(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $id = $data['id'];
        unset($data['id']);
        $response = $this->walikelas->update_walikelas($id, $data);
        echo $response;
    }
    public function delete_walikelas(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $id = $data['id'];
        $response = $this->walikelas->delete_walikelas($id);
        echo $response;
    }
}
