<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Models\Authorization as Authoriation_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as FacadesValidator;

class Authorization extends BaseController
{

    public function __construct(Authoriation_Model $authorization)
    {
        $this->authorization = $authorization;
    }

    public function signin()
    {
        return view('login');
    }
    public function signout()
    {
        session()->pull('sess_user');
        return redirect('login')->with(['success' => 'Logout Berhasil']);
    }

    public function check(Request $request)
    {
        $data = $request->only('a', 'b');
        // $validator = FacadesValidator::make($request->all(), [
        //     'a' => 'required|min:4',
        //     'b' => 'required|min:4',
        // ]);
        // if ($validator->fails()) {
        //     echo json_encode($validator->errors());
        // }
        $check = $this->authorization->check_login($data);
        echo json_encode($check);
    }
}
