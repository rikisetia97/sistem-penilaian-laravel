<?php

namespace App\Http\Controllers\Absensi;

use App\Http\Controllers\Core_Controller;
use App\Models\Absensi\Absensi as Absensi_Model;
use Illuminate\Http\Request;

class Absensi extends Core_Controller
{

    public function __construct(Absensi_Model $absensi)
    {
        parent::__construct();
        $this->absensi = $absensi;
    }

    public function index()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $siswa = $this->absensi->get_all_data('siswa');
        $kelas = $this->absensi->get_all_data('kelas');
        $pelajaran = $this->absensi->get_all_data('pelajaran');
        $semester = $this->absensi->get_all_data('semester');
        $jurusan = $this->absensi->get_all_data('jurusan');
        $tahun_ajaran = $this->absensi->get_all_data('tahun_ajaran');
        return view('absensi/index', ['siswa' => $siswa, 'kelas' => $kelas, 'pelajaran' => $pelajaran, 'tahun_ajaran' => $tahun_ajaran, 'semester' => $semester, 'jurusan' => $jurusan]);
    }

    public function siswa()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $siswa = $this->absensi->get_all_data('siswa');
        $kelas = $this->absensi->get_all_data('kelas');
        $pelajaran = $this->absensi->get_all_data('pelajaran');
        $semester = $this->absensi->get_all_data('semester');
        $jurusan = $this->absensi->get_all_data('jurusan');
        $tahun_ajaran = $this->absensi->get_all_data('tahun_ajaran');
        return view('absensi/siswa', ['siswa' => $siswa, 'kelas' => $kelas, 'pelajaran' => $pelajaran, 'tahun_ajaran' => $tahun_ajaran, 'semester' => $semester, 'jurusan' => $jurusan]);
    }

    public function persiswa()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $siswa = $this->absensi->get_all_data('siswa');
        $kelas = $this->absensi->get_all_data('kelas');
        $pelajaran = $this->absensi->get_all_data('pelajaran');
        $semester = $this->absensi->get_all_data('semester');
        $jurusan = $this->absensi->get_all_data('jurusan');
        $tahun_ajaran = $this->absensi->get_all_data('tahun_ajaran');
        return view('absensi/persiswa', ['siswa' => $siswa, 'kelas' => $kelas, 'pelajaran' => $pelajaran, 'tahun_ajaran' => $tahun_ajaran, 'semester' => $semester, 'jurusan' => $jurusan]);
    }
    public function get_absensi_persiswa(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_absensi = $this->absensi->get_absensi($data, session()->get('sess_user')['id']);
        echo json_encode($get_absensi);
    }

    public function get_absensi_siswa(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_absensi = $this->absensi->get_absensi($data);
        echo json_encode($get_absensi);
    }

    public function get_absensi_input(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_absensi = $this->absensi->get_absensi_input($data);
        echo json_encode($get_absensi);
    }
    public function post_absensi_input(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_absensi = $this->absensi->post_absensi_input($data);
        echo json_encode($get_absensi);
    }

    public function input_absensi(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_absensi = $this->absensi->input_absensi($data);
        echo json_encode($get_absensi);
    }
}
