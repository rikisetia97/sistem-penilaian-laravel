<?php

namespace App\Http\Controllers\Guru;

use App\Http\Controllers\Core_Controller;
use App\Models\Guru\Guru as Guru_Model;
use Illuminate\Http\Request;

class Guru extends Core_Controller
{

    public function __construct(Guru_Model $guru)
    {
        parent::__construct();
        $this->guru = $guru;
    }

    public function index()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        return view('guru/index');
    }

    function get_guru()
    {
        $guru = $this->guru->get_guru();
        echo json_encode($guru);
    }

    public function insert_guru(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $data['password'] = str_replace('-', '', date('d-m-Y', strtotime($data['tgl_lahir'])));
        $data['role'] = 'guru';
        $response = $this->guru->insert_guru($data);
        echo $response;
    }
    public function update_guru(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $id = $data['id'];
        unset($data['id']);
        $data['password'] = str_replace('-', '', date('d-m-Y', strtotime($data['tgl_lahir'])));
        $response = $this->guru->update_guru($id, $data);
        echo $response;
    }
    public function delete_guru(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $id = $data['id'];
        $response = $this->guru->delete_guru($id);
        echo $response;
    }
}
