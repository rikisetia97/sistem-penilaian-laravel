<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;

class Core_Controller extends BaseController
{

    public function __construct()
    {
    }

    public function check_session()
    {
        $check = session()->get('sess_user');
        if (!empty($check['role'])) {
            return true;
        }
        return false;
    }
}
