<?php

namespace App\Http\Controllers\Rekap;

use App\Http\Controllers\Core_Controller;
use App\Models\Rekap\Rekap as Rekap_Model;
use Illuminate\Http\Request;

class Rekap extends Core_Controller
{

    public function __construct(Rekap_Model $rekap)
    {
        parent::__construct();
        $this->rekap = $rekap;
    }

    public function raport()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $kelas = $this->rekap->get_all_data('kelas');
        $pelajaran = $this->rekap->get_all_data('pelajaran');
        $semester = $this->rekap->get_all_data('semester');
        $jurusan = $this->rekap->get_all_data('jurusan');
        $tahun_ajaran = $this->rekap->get_all_data('tahun_ajaran');
        return view('rekap/raport', ['kelas' => $kelas, 'pelajaran' => $pelajaran, 'tahun_ajaran' => $tahun_ajaran, 'semester' => $semester, 'jurusan' => $jurusan]);
    }

    public function rekap_nilai()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $kelas = $this->rekap->get_all_data('kelas');
        $pelajaran = $this->rekap->get_all_data('pelajaran');
        $semester = $this->rekap->get_all_data('semester');
        $jurusan = $this->rekap->get_all_data('jurusan');
        $tahun_ajaran = $this->rekap->get_all_data('tahun_ajaran');
        return view('rekap/raport', ['kelas' => $kelas, 'pelajaran' => $pelajaran, 'tahun_ajaran' => $tahun_ajaran, 'semester' => $semester, 'jurusan' => $jurusan]);
    }

    public function get_nilai_rekap(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_nilai = $this->rekap->get_nilai_rekap($data);
        echo json_encode($get_nilai);
    }

    public function cetak_raport($id)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $get_nilai = $this->rekap->cetak_raport($id);
        echo json_encode($get_nilai);
    }
    public function cetak_absensi($id)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $get_nilai = $this->rekap->cetak_absensi($id);
        echo json_encode($get_nilai);
    }

    public function get_siswa_rekap(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        if (session()->get('sess_user')['role'] == 'guru') {
            $check = $this->rekap->get_walikelas(session()->get('sess_user')['id']);
            $data['id_kelas'] = $check->id_kelas;
            $data['id_jurusan'] = $check->id_jurusan;
        }
        $get_nilai = $this->rekap->get_siswa_rekap($data);
        echo json_encode($get_nilai);
    }
}
