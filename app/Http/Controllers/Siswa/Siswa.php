<?php

namespace App\Http\Controllers\Siswa;

use App\Http\Controllers\Core_Controller;
use App\Models\Siswa\Siswa as Siswa_Model;
use Illuminate\Http\Request;

class Siswa extends Core_Controller
{

    public function __construct(Siswa_Model $siswa)
    {
        parent::__construct();
        $this->siswa = $siswa;
    }

    public function index()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $kelas = $this->siswa->get_all_data('kelas');
        $jurusan = $this->siswa->get_all_data('jurusan');
        return view('siswa/index', ['kelas' => $kelas, 'jurusan' => $jurusan]);
    }

    function get_siswa()
    {
        $siswa = $this->siswa->get_siswa();
        echo json_encode($siswa);
    }

    public function insert_siswa(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $data['password'] = str_replace('-', '', date('d-m-Y', strtotime($data['tgl_lahir'])));
        $response = $this->siswa->insert_siswa($data);
        echo $response;
    }
    public function update_siswa(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $data['password'] = str_replace('-', '', date('d-m-Y', strtotime($data['tgl_lahir'])));
        $id = $data['id'];
        unset($data['id']);
        $response = $this->siswa->update_siswa($id, $data);
        echo $response;
    }
    public function delete_siswa(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $id = $data['id'];
        $response = $this->siswa->delete_siswa($id);
        echo $response;
    }
}
