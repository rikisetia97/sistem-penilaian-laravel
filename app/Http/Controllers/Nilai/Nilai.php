<?php

namespace App\Http\Controllers\Nilai;

use App\Http\Controllers\Core_Controller;
use App\Models\Nilai\Nilai as Nilai_Model;
use Illuminate\Http\Request;

class Nilai extends Core_Controller
{

    public function __construct(Nilai_Model $nilai)
    {
        parent::__construct();
        $this->nilai = $nilai;
    }

    public function index()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $kelas = $this->nilai->get_all_data('kelas');
        $pelajaran = $this->nilai->get_all_data('pelajaran');
        $semester = $this->nilai->get_all_data('semester');
        $jurusan = $this->nilai->get_all_data('jurusan');
        $tahun_ajaran = $this->nilai->get_all_data('tahun_ajaran');
        return view('nilai/index', ['kelas' => $kelas, 'pelajaran' => $pelajaran, 'tahun_ajaran' => $tahun_ajaran, 'semester' => $semester, 'jurusan' => $jurusan]);
    }

    public function siswa()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $kelas = $this->nilai->get_all_data('kelas');
        $pelajaran = $this->nilai->get_all_data('pelajaran');
        $semester = $this->nilai->get_all_data('semester');
        $jurusan = $this->nilai->get_all_data('jurusan');
        $tahun_ajaran = $this->nilai->get_all_data('tahun_ajaran');
        return view('nilai/siswa', ['kelas' => $kelas, 'pelajaran' => $pelajaran, 'tahun_ajaran' => $tahun_ajaran, 'semester' => $semester, 'jurusan' => $jurusan]);
    }
    public function persiswa()
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $kelas = $this->nilai->get_all_data('kelas');
        $pelajaran = $this->nilai->get_all_data('pelajaran');
        $semester = $this->nilai->get_all_data('semester');
        $jurusan = $this->nilai->get_all_data('jurusan');
        $tahun_ajaran = $this->nilai->get_all_data('tahun_ajaran');
        return view('nilai/persiswa', ['kelas' => $kelas, 'pelajaran' => $pelajaran, 'tahun_ajaran' => $tahun_ajaran, 'semester' => $semester, 'jurusan' => $jurusan]);
    }

    public function get_nilai_siswa(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_nilai = $this->nilai->get_nilai($data);
        echo json_encode($get_nilai);
    }
    public function get_nilai_persiswa(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_nilai = $this->nilai->get_nilai($data, session()->get('sess_user')['id']);
        echo json_encode($get_nilai);
    }

    public function update_nilai_siswa(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_nilai = $this->nilai->update_nilai($data);
        echo json_encode($get_nilai);
    }

    public function delete_nilai_siswa(Request $request)
    {
        if (!$this->check_session()) {
            return redirect('login');
        }
        $data = $request->all();
        unset($data['_token']);
        $get_nilai = $this->nilai->delete_nilai($data);
        echo json_encode($get_nilai);
    }
}
