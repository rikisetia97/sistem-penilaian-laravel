<?php

namespace App\Models\Nilai;

use App\Models\Core_Model;
use Illuminate\Support\Facades\DB;


class Nilai extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_data($type)
    {
        $query = DB::table("tm_$type")->get();
        return $query;
    }

    public function get_nilai($data, $id_siswa = '')
    {
        extract($data);
        $where = [];
        if (@$id_siswa) {
            $where['tm_penilaian.id_siswa'] = $id_siswa;
        }
        if (@$id_kelas) {
            $where['tm_penilaian.id_kelas'] = $id_kelas;
        }
        if (@$id_jurusan) {
            $where['tm_penilaian.id_jurusan'] = $id_jurusan;
        }
        if (@$id_semester) {
            $where['tm_penilaian.id_semester'] = $id_semester;
        }
        if (@$id_pelajaran) {
            $where['tm_penilaian.id_pelajaran'] = $id_pelajaran;
        }
        if (@$id_tahun_ajaran) {
            $where['tm_penilaian.id_tahun_ajaran'] = $id_tahun_ajaran;
        }
        $users = DB::table('tm_penilaian')
            ->select(
                'tm_penilaian.id',
                'tm_siswa.nisn',
                'tm_siswa.nama',
                'tm_kelas.kelas',
                'tm_pelajaran.pelajaran',
                'tm_tahun_ajaran.tahun_ajaran',
                'tm_jurusan.jurusan',
                'tm_semester.semester',
                'tm_penilaian.kkm',
                'tm_penilaian.nilai'
            )
            ->where($where)
            ->join('tm_siswa', 'tm_siswa.id', '=', 'tm_penilaian.id_siswa')
            ->join('tm_kelas', 'tm_kelas.id', '=', 'tm_penilaian.id_kelas')
            ->join('tm_pelajaran', 'tm_pelajaran.id', '=', 'tm_penilaian.id_pelajaran')
            ->join('tm_tahun_ajaran', 'tm_tahun_ajaran.id', '=', 'tm_penilaian.id_tahun_ajaran')
            ->join('tm_semester', 'tm_semester.id', '=', 'tm_penilaian.id_semester')
            ->join('tm_jurusan', 'tm_jurusan.id', '=', 'tm_penilaian.id_jurusan')
            ->get();
        return $users;
    }

    public function update_nilai($data)
    {
        extract($data);
        $query = DB::table('tm_penilaian')->where(['id' => $id])->update(['nilai' => $nilai]);
        return $query;
    }
    public function delete_nilai($data)
    {
        extract($data);
        $query = DB::table('tm_penilaian')->where(['id' => $id])->delete();
        return $query;
    }
}
