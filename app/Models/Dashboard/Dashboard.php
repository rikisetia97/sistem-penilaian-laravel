<?php

namespace App\Models\Dashboard;

use App\Models\Core_Model;
use Illuminate\Support\Facades\DB;


class Dashboard extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_widget()
    {
        // $users = DB::select("select * from tm_admin where id = :id and username = :username ", [
        //     'id' => 1,
        //     'username' => 'admin'
        // ]);
        return '';
    }
}
