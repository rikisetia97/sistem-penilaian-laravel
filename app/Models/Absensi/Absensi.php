<?php

namespace App\Models\Absensi;

use App\Models\Core_Model;
use Illuminate\Support\Facades\DB;


class Absensi extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_data($type)
    {
        $query = DB::table("tm_$type")->get();
        return $query;
    }

    public function get_absensi($data, $id_siswa = '')
    {
        extract($data);
        $where = [];
        if (@$id_siswa) {
            $where['tm_absen.id_siswa'] = $id_siswa;
        }
        if (@$id_kelas) {
            $where['tm_absen.id_kelas'] = $id_kelas;
        }
        if (@$id_jurusan) {
            $where['tm_absen.id_jurusan'] = $id_jurusan;
        }
        if (@$id_semester) {
            $where['tm_absen.id_semester'] = $id_semester;
        }
        if (@$id_pelajaran) {
            $where['tm_absen.id_pelajaran'] = $id_pelajaran;
        }
        if (@$id_tahun_ajaran) {
            $where['tm_absen.id_tahun_ajaran'] = $id_tahun_ajaran;
        }
        $users = DB::table('tm_absen')
            ->select(
                'tm_siswa.nisn',
                'tm_siswa.nama',
                'tm_kelas.kelas',
                'tm_pelajaran.pelajaran',
                'tm_tahun_ajaran.tahun_ajaran',
                'tm_jurusan.jurusan',
                'tm_semester.semester',
                DB::raw('
                    SUM(hadir) hadir, 
                    SUM(ijin) ijin,
                    SUM(alpha) alpha,
                    SUM(sakit) sakit
                ')
            )
            ->where($where)
            ->join('tm_siswa', 'tm_siswa.id', '=', 'tm_absen.id_siswa')
            ->join('tm_kelas', 'tm_kelas.id', '=', 'tm_absen.id_kelas')
            ->join('tm_pelajaran', 'tm_pelajaran.id', '=', 'tm_absen.id_pelajaran')
            ->join('tm_tahun_ajaran', 'tm_tahun_ajaran.id', '=', 'tm_absen.id_tahun_ajaran')
            ->join('tm_semester', 'tm_semester.id', '=', 'tm_absen.id_semester')
            ->join('tm_jurusan', 'tm_jurusan.id', '=', 'tm_absen.id_jurusan')
            ->groupBy(
                'tm_siswa.nisn',
                'tm_siswa.nama',
                'tm_kelas.kelas',
                'tm_pelajaran.pelajaran',
                'tm_tahun_ajaran.tahun_ajaran',
                'tm_jurusan.jurusan',
                'tm_semester.semester',
                'tm_absen.id_siswa',
                'tm_absen.id_jurusan',
                'tm_absen.id_kelas',
                'tm_absen.id_semester',
                'tm_absen.id_pelajaran',
                'tm_absen.id_tahun_ajaran'
            )
            ->get();
        return $users;
    }
    public function get_absensi_input($data)
    {
        extract($data);
        $where = [];
        if (isset($id_kelas)) {
            $where['tm_kelas.id'] = $id_kelas;
        }
        if (isset($id_jurusan)) {
            $where['tm_jurusan.id'] = $id_jurusan;
        }
        $users = DB::table('tm_siswa')
            ->select('tm_siswa.id', 'nama', 'nisn')
            ->where($where)
            ->join('tm_kelas', 'tm_kelas.id', '=', 'tm_siswa.id_kelas')
            ->join('tm_jurusan', 'tm_jurusan.id', '=', 'tm_siswa.id_jurusan')
            ->get();
        return $users;
    }

    public function post_absensi_input($data)
    {
        extract($data);
        $data_insert = [];
        $i = 0;
        foreach ($id_siswa as $item) {
            $insert = [
                'id_siswa' => $id_siswa[$i],
                'id_jurusan' => $id_jurusan,
                'id_pelajaran' => $id_pelajaran,
                'id_kelas' => $id_kelas,
                'id_tahun_ajaran' => $id_tahun_ajaran,
                'id_semester' => $id_semester,
                'kkm' => $kkm[$i],
                'nilai' => $nilai[$i]
            ];
            $data_insert[] = $insert;
            $i++;
        }
        $affected = DB::table('tm_penilaian')->insert($data_insert);
        return $affected;
    }
    public function input_absensi($data)
    {
        // foreach ($data['data'] as $item) {
        //     unset($item['hadir']);
        //     unset($item['alpha']);
        //     unset($item['ijin']);
        //     unset($item['sakit']);
        //     dd(DB::table('tm_absen')->where($item)->whereRaw('date_format(created_at, "%Y-%m-%d")',  '2021-12-12')->first());
        // }

        $affected = DB::table('tm_absen')->insert($data['data']);
        return $affected;
    }
}
