<?php

namespace App\Models\Rekap;

use App\Models\Core_Model;
use Illuminate\Support\Facades\DB;
use Codedge\Fpdf\Fpdf\Fpdf;

class Rekap extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_data($type)
    {
        $query = DB::table("tm_$type")->get();
        return $query;
    }

    public function get_nilai_rekap($data)
    {
        extract($data);
        $where['tm_penilaian.id_siswa'] = $id_siswa;
        $where['tm_penilaian.id_semester'] = $id_semester;
        $where['tm_penilaian.id_tahun_ajaran'] = $id_tahun_ajaran;
        $users = DB::table('tm_penilaian')
            ->select(
                'tm_penilaian.id',
                'tm_siswa.nisn',
                'tm_siswa.nama',
                'tm_kelas.kelas',
                'tm_pelajaran.pelajaran',
                'tm_tahun_ajaran.tahun_ajaran',
                'tm_jurusan.jurusan',
                'tm_semester.semester',
                'tm_penilaian.kkm',
                'tm_penilaian.nilai'
            )
            ->where($where)
            ->join('tm_siswa', 'tm_siswa.id', '=', 'tm_penilaian.id_siswa')
            ->join('tm_kelas', 'tm_kelas.id', '=', 'tm_penilaian.id_kelas')
            ->join('tm_pelajaran', 'tm_pelajaran.id', '=', 'tm_penilaian.id_pelajaran')
            ->join('tm_tahun_ajaran', 'tm_tahun_ajaran.id', '=', 'tm_penilaian.id_tahun_ajaran')
            ->join('tm_semester', 'tm_semester.id', '=', 'tm_penilaian.id_semester')
            ->join('tm_jurusan', 'tm_jurusan.id', '=', 'tm_penilaian.id_jurusan')
            ->get();
        return $users;
    }

    public function get_siswa_rekap($data)
    {
        extract($data);
        if (@$id_kelas) {
            $where['tm_siswa.id_kelas'] = $id_kelas;
        }
        if (@$id_jurusan) {
            $where['tm_siswa.id_jurusan'] = $id_jurusan;
        }
        $query = DB::table("tm_siswa")
            ->select(
                'tm_siswa.id',
                'tm_siswa.nisn',
                'tm_siswa.nama',
                'tm_siswa.alamat',
                'tm_siswa.nomor_hp',
                'tm_siswa.gender',
                'tm_siswa.created_at',
                'tm_kelas.kelas',
                'tm_jurusan.jurusan'
            )
            ->where($where)
            ->join('tm_kelas', 'tm_kelas.id', '=', 'tm_siswa.id_kelas')
            ->join('tm_jurusan', 'tm_jurusan.id', '=', 'tm_siswa.id_jurusan')
            ->get();
        return $query;
    }
    public function get_walikelas($id)
    {
        $query = DB::table("tm_walikelas")
            ->where([
                'id_guru' => $id
            ])
            ->first();
        return $query;
    }

    public function _cetak_raport($id)
    {
        $where['tm_penilaian.id_siswa'] = $id;
        $data = DB::table('tm_penilaian')
            ->select(
                'tm_penilaian.id',
                'tm_siswa.nisn',
                'tm_siswa.nama',
                'tm_kelas.kelas',
                'tm_pelajaran.pelajaran',
                'tm_tahun_ajaran.tahun_ajaran',
                'tm_jurusan.jurusan',
                'tm_semester.semester',
                'tm_penilaian.kkm',
                'tm_penilaian.nilai'
            )
            ->where($where)
            ->join('tm_siswa', 'tm_siswa.id', '=', 'tm_penilaian.id_siswa')
            ->join('tm_kelas', 'tm_kelas.id', '=', 'tm_penilaian.id_kelas')
            ->join('tm_pelajaran', 'tm_pelajaran.id', '=', 'tm_penilaian.id_pelajaran')
            ->join('tm_tahun_ajaran', 'tm_tahun_ajaran.id', '=', 'tm_penilaian.id_tahun_ajaran')
            ->join('tm_semester', 'tm_semester.id', '=', 'tm_penilaian.id_semester')
            ->join('tm_jurusan', 'tm_jurusan.id', '=', 'tm_penilaian.id_jurusan')
            ->get();
        $arr = array();

        foreach ($data as $key => $item) {
            if ($item->nilai >= 85 && $item->nilai <= 100) {
                $predikat = 'A';
            } else if ($item->nilai >= 75 && $item->nilai < 85) {
                $predikat = 'B';
            } else if ($item->nilai >= 60 && $item->nilai < 75) {
                $predikat = 'C';
            } else if ($item->nilai < 60) {
                $predikat = 'D';
            }
            $item->predikat = $predikat;
            $arr[$item->tahun_ajaran][$key] = $item;
        }

        ksort($arr, SORT_NUMERIC);


        $results = [];
        foreach ($arr as $items) {
            $new_smt = [];
            foreach ($items as $key => $item) {
                $new_smt[$item->semester][$key] = $item;
            }

            ksort($items, SORT_NUMERIC);
            $_smt = [];
            foreach ($new_smt as $smt) {
                $_smt[] = $smt;
            }
            $results[] = $_smt;
        }
        return $results;
    }
    public function _cetak_absensi($id)
    {
        $where['tm_absen.id_siswa'] = $id;
        $data = DB::table('tm_absen')
            ->select(
                'tm_siswa.nisn',
                'tm_siswa.nama',
                'tm_kelas.kelas',
                'tm_pelajaran.pelajaran',
                'tm_tahun_ajaran.tahun_ajaran',
                'tm_jurusan.jurusan',
                'tm_semester.semester',
                DB::raw('
                    SUM(hadir) hadir, 
                    SUM(ijin) ijin,
                    SUM(alpha) alpha,
                    SUM(sakit) sakit
                ')
            )
            ->where($where)
            ->join('tm_siswa', 'tm_siswa.id', '=', 'tm_absen.id_siswa')
            ->join('tm_kelas', 'tm_kelas.id', '=', 'tm_absen.id_kelas')
            ->join('tm_pelajaran', 'tm_pelajaran.id', '=', 'tm_absen.id_pelajaran')
            ->join('tm_tahun_ajaran', 'tm_tahun_ajaran.id', '=', 'tm_absen.id_tahun_ajaran')
            ->join('tm_semester', 'tm_semester.id', '=', 'tm_absen.id_semester')
            ->join('tm_jurusan', 'tm_jurusan.id', '=', 'tm_absen.id_jurusan')
            ->groupBy(
                'tm_siswa.nisn',
                'tm_siswa.nama',
                'tm_kelas.kelas',
                'tm_pelajaran.pelajaran',
                'tm_tahun_ajaran.tahun_ajaran',
                'tm_jurusan.jurusan',
                'tm_semester.semester',
                'tm_absen.id_siswa',
                'tm_absen.id_jurusan',
                'tm_absen.id_kelas',
                'tm_absen.id_semester',
                'tm_absen.id_pelajaran',
                'tm_absen.id_tahun_ajaran'
            )
            ->get();
        $arr = array();

        foreach ($data as $key => $item) {
            $arr[$item->tahun_ajaran][$key] = $item;
        }

        ksort($arr, SORT_NUMERIC);


        $results = [];
        foreach ($arr as $items) {
            $new_smt = [];
            foreach ($items as $key => $item) {
                $new_smt[$item->semester][$key] = $item;
            }

            ksort($items, SORT_NUMERIC);
            $_smt = [];
            foreach ($new_smt as $smt) {
                $_smt[] = $smt;
            }
            $results[] = $_smt;
        }
        return $results;
    }
    public function cetak_raport($id)
    {
        $data = $this->_cetak_raport($id);
        // $data = (object) $data;
        $pdf = new FPDF;
        $nama = '';
        foreach ($data as $item) {
            foreach ($item as $item_smt) {
                $item_smt = array_values($item_smt);
                $nama = $item_smt[0]->nama;
                $pdf->AddPage();
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(10, 6, "Nama                                  : " . $item_smt[0]->nama . "", 0, 1);
                $pdf->Cell(10, 6, "NIS / NISN                          : " . $item_smt[0]->nisn . "", 0, 1);
                $pdf->Cell(100, 6, "Kelas                                   : " . $item_smt[0]->kelas . "", 0, 1);
                $pdf->Cell(100, 6, "Jurusan                               : " . $item_smt[0]->jurusan . "", 0, 1);
                $pdf->Cell(100, 6, "Semester / Tahun Ajaran    : " . $item_smt[0]->semester . " - " . $item_smt[0]->tahun_ajaran . "", 0, 1);
                $pdf->Line(10, 45, 220 - 20, 45);
                $pdf->Line(60, 45, 220 - 50, 45);
                $pdf->Line(10, 56, 220 - 20, 56);
                $pdf->Line(60, 56, 220 - 50, 56);
                $pdf->Cell(5, 7, '', 0, 1);
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->Cell(190, 7, 'PENCAPAIAN KOMPETENSI PESERTA DIDIK', 0, 1, 'C');
                // Memberikan space kebawah agar tidak terlalu rapat
                $pdf->Cell(10, 7, '', 0, 1);


                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(100, 6, "A. Nilai Akademik", 0, 1);
                $pdf->Cell(10, 6, 'No.', 1, 0);
                $pdf->Cell(90, 6, 'Mata Pelajaran', 1, 0);
                $pdf->Cell(40, 6, 'Nilai KKM', 1, 0);
                $pdf->Cell(27, 6, 'Nilai', 1, 0);
                $pdf->Cell(25, 6, 'Predikat', 1, 1);
                $pdf->SetFont('Arial', '', 10);
                $no = 1;
                foreach ($item_smt as $key) {
                    $pdf->Cell(10, 6, $no++, 1, 0);
                    $pdf->Cell(90, 6, $key->pelajaran, 1, 0);
                    $pdf->Cell(40, 6, $key->kkm, 1, 0);
                    $pdf->Cell(27, 6, $key->nilai, 1, 0);
                    $pdf->Cell(25, 6, $key->predikat, 1, 1);
                    // $pdf->Cell(25, 6, $key['predikat'], 1, 1);
                }
            }
        }

        $pdf->Output('D', 'RekapNilai-' . $nama . '.pdf');
    }
    public function cetak_absensi($id)
    {
        $data = $this->_cetak_absensi($id);
        // $data = (object) $data;
        $pdf = new FPDF;
        $nama = '';
        foreach ($data as $item) {
            foreach ($item as $item_smt) {
                $item_smt = array_values($item_smt);
                $nama = $item_smt[0]->nama;
                $pdf->AddPage();
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(10, 6, "Nama                                  : " . $item_smt[0]->nama . "", 0, 1);
                $pdf->Cell(10, 6, "NIS / NISN                          : " . $item_smt[0]->nisn . "", 0, 1);
                $pdf->Cell(100, 6, "Kelas                                   : " . $item_smt[0]->kelas . "", 0, 1);
                $pdf->Cell(100, 6, "Jurusan                               : " . $item_smt[0]->jurusan . "", 0, 1);
                $pdf->Cell(100, 6, "Semester / Tahun Ajaran    : " . $item_smt[0]->semester . " - " . $item_smt[0]->tahun_ajaran . "", 0, 1);
                $pdf->Line(10, 45, 220 - 20, 45);
                $pdf->Line(60, 45, 220 - 50, 45);
                $pdf->Line(10, 56, 220 - 20, 56);
                $pdf->Line(60, 56, 220 - 50, 56);
                $pdf->Cell(5, 7, '', 0, 1);
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->Cell(190, 7, 'PENCAPAIAN KOMPETENSI PESERTA DIDIK', 0, 1, 'C');
                // Memberikan space kebawah agar tidak terlalu rapat
                $pdf->Cell(10, 7, '', 0, 1);


                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(100, 6, "B. Rekap Absensi", 0, 1);
                $pdf->Cell(10, 6, 'No.', 1, 0);
                $pdf->Cell(100, 6, 'Mata Pelajaran', 1, 0);
                $pdf->Cell(20, 6, 'Hadir', 1, 0);
                $pdf->Cell(20, 6, 'Ijin', 1, 0);
                $pdf->Cell(20, 6, 'Alpha', 1, 0);
                $pdf->Cell(20, 6, 'Sakit', 1, 1);
                $pdf->SetFont('Arial', '', 10);
                $no = 1;
                foreach ($item_smt as $key) {
                    $pdf->Cell(10, 6, $no++, 1, 0);
                    $pdf->Cell(100, 6, $key->pelajaran, 1, 0);
                    $pdf->Cell(20, 6, $key->hadir, 1, 0);
                    $pdf->Cell(20, 6, $key->ijin, 1, 0);
                    $pdf->Cell(20, 6, $key->alpha, 1, 0);
                    $pdf->Cell(20, 6, $key->sakit, 1, 1);
                    // $pdf->Cell(25, 6, $key['predikat'], 1, 1);
                }
            }
        }

        $pdf->Output('D', 'RekapNilai-' . $nama . '.pdf');
    }
}
