<?php

namespace App\Models;

use App\Models\Core_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class Authorization extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function check_login($data)
    {
        $n = false;
        $p = false;
        $nip = DB::table('tm_users')->where([
            ['nip', $data['a']],
        ])->get()->first();
        if ($nip) {
            $n = true;
            $user = DB::table('tm_users')->where([
                ['nip', $data['a']],
                ['password', $data['b']],
            ])->get()->first();
            if ($user) {
                $p = true;
                session()->put('sess_user', [
                    'id' => $user->id,
                    'nip' => $user->nip,
                    'nama' => $user->nama,
                    'nomor_hp' => $user->nomor_hp,
                    'gender' => $user->gender,
                    'role' => $user->role,
                    'created_at' => $user->created_at
                ]);
            }
        } else {
            $nisn = DB::table('tm_siswa')->where([
                ['nisn', $data['a']],
            ])->get()->first();
            if ($nisn) {
                $n = true;
                $user = DB::table('tm_siswa')->where([
                    ['nisn', $data['a']],
                    ['password', $data['b']],
                ])->get()->first();
                if ($user) {
                    $p = true;
                    session()->put('sess_user', [
                        'id' => $user->id,
                        'nip' => $user->nisn,
                        'nama' => $user->nama,
                        'nomor_hp' => $user->nomor_hp,
                        'gender' => $user->gender,
                        'role' => 'siswa',
                        'created_at' => $user->created_at
                    ]);
                }
            }
        }


        return ['n' => $n, 'p' => $p];
    }
}
