<?php

namespace App\Models\Siswa;

use App\Models\Core_Model;
use Illuminate\Support\Facades\DB;


class Siswa extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_data($type)
    {
        $query = DB::table("tm_$type")->get();
        return $query;
    }

    public function get_siswa()
    {
        $query = DB::table("tm_siswa as s")
            ->select('s.*', 'j.jurusan', 'k.kelas')
            ->join("tm_jurusan as j", "s.id_jurusan", "=", "j.id")
            ->join("tm_kelas as k", "s.id_kelas", "=", "k.id")
            ->get();
        return $query;
    }
    public function insert_siswa($data)
    {
        $query = DB::table("tm_siswa")
            ->insert($data);
        return $query;
    }
    public function update_siswa($id, $data)
    {
        $query = DB::table("tm_siswa")
            ->where(['id' => $id])
            ->update($data);
        return $query;
    }
    public function delete_siswa($id)
    {
        $query = DB::table("tm_siswa")
            ->where(['id' => $id])
            ->delete();
        return $query;
    }
}
