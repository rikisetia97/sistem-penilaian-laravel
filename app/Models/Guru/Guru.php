<?php

namespace App\Models\Guru;

use App\Models\Core_Model;
use Illuminate\Support\Facades\DB;


class Guru extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_guru()
    {
        $query = DB::table("tm_users as u")
            ->where('role', '!=', 'admin')
            ->get();
        return $query;
    }
    public function insert_guru($data)
    {
        $query = DB::table("tm_users")
            ->insert($data);
        return $query;
    }
    public function update_guru($id, $data)
    {
        $query = DB::table("tm_users")
            ->where(['id' => $id])
            ->update($data);
        return $query;
    }
    public function delete_guru($id)
    {
        $query = DB::table("tm_users")
            ->where(['id' => $id])
            ->delete();
        return $query;
    }
}
