<?php

namespace App\Models\Akademik;

use App\Models\Core_Model;
use Illuminate\Support\Facades\DB;


class Akademik extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_akademik($type)
    {
        $query = DB::table("tm_$type")->get();
        return $query;
    }

    public function add($data, $type)
    {
        $query = DB::table("tm_$type")->insert([$type => $data['input_akademik']]);
        return $query;
    }

    public function edit($data, $type)
    {
        $query = DB::table("tm_$type")->where(['id' => $data['id']])->update([$type => $data['input_akademik']]);
        return $query;
    }

    public function delet($data, $type)
    {
        $query = DB::table("tm_$type")->where(['id' => $data['id']])->delete();
        return $query;
    }
}
