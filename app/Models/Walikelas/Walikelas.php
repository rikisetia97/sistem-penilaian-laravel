<?php

namespace App\Models\Walikelas;

use App\Models\Core_Model;
use Illuminate\Support\Facades\DB;


class Walikelas extends Core_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_data($type)
    {
        if ($type == 'users') {
            $query = DB::table("tm_$type")->where(['role' => 'guru'])->get();
        } else {
            $query = DB::table("tm_$type")->get();
        }
        return $query;
    }

    public function get_walikelas()
    {
        $query = DB::table("tm_walikelas as w")
            ->select('w.*', 'k.kelas', 'j.jurusan', 'u.nama', 'u.nip')
            ->join('tm_kelas as k', 'k.id', '=', 'w.id_kelas')
            ->join('tm_jurusan as j', 'j.id', '=', 'w.id_jurusan')
            ->join('tm_users as u', 'u.id', '=', 'w.id_guru')
            ->get();
        return $query;
    }
    public function insert_walikelas($data)
    {
        $check = DB::table("tm_walikelas")->where($data)->first();
        if (!$check) {
            $query = DB::table("tm_walikelas")
                ->insert($data);
            return $query;
        }
        return false;
    }
    public function update_walikelas($id, $data)
    {
        $query = DB::table("tm_walikelas")
            ->where(['id' => $id])
            ->update($data);
        return $query;
    }
    public function delete_walikelas($id)
    {
        $query = DB::table("tm_walikelas")
            ->where(['id' => $id])
            ->delete();
        return $query;
    }
}
