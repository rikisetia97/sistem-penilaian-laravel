<?php

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;

if (!function_exists('rnd')) {
    function rnd($leng = 16, $str = false)
    {
        $use = "1234567891234567890";
        if ($str) {
            $use .= 'ABCDEFGHIJKLMNOPQRTUVWXYZ';
        }
        srand((float) microtime() * 1000000);
        $api = '';
        for ($i = 0; $i < $leng; $i++) {
            $api .= $use[rand() % strlen($use)];
        }
        return $api;
    }
}

if (!function_exists('breadcrumb_desktop')) {
    function breadcrumb_desktop($data)
    {
        $html = '<div class="header-holder header-holder-desktop"> <div class="header-container container-fluid"> <h4 class="header-title">' . $data['title'] . '</h4><i class="header-divider"></i> <div class="header-wrap header-wrap-block justify-content-start"> <div class="breadcrumb"><a href="javascript:void(0)" class="breadcrumb-item"> <div class="breadcrumb-icon"><i class="' . $data['icon'] . '"></i></div><span class="breadcrumb-text">' . $data['title'] . '</span> </a></div> </div> <div class="header-wrap"><button class="btn btn-label-info btn-icon ml-2" id="fullscreen-trigger" data-toggle="tooltip" title="Toggle fullscreen" data-placement="left"><i class="fa fa-expand fullscreen-icon-expand"></i> <i class="fa fa-compress fullscreen-icon-compress"></i></button> </div> </div> </div>';
        return $html;
    }
}

if (!function_exists('breadcrumb_mobile')) {
    function breadcrumb_mobile($data)
    {
        $html = '<div class="header-holder header-holder-mobile"> <div class="header-container container-fluid"> <div class="header-wrap header-wrap-block justify-content-start w-100"> <div class="breadcrumb"><a href="javascript:void(0)" class="breadcrumb-item"> <div class="breadcrumb-icon"><i class="' . $data['icon'] . '"></i></div><span class="breadcrumb-text">' . $data['title'] . '</span> </a></div> </div> </div> </div>';
        return $html;
    }
}
if (!function_exists('check_active_menu')) {
    function check_active_menu($param1, $param2)
    {
        if (@$param2) {
            if ($param1 . '/' . $param2 == Request::segment(1) . '/' . Request::segment(2)) {
                return 'active';
            }
        } else {
            if ($param1 == Request::segment(1)) {
                return '';
            }
        }
    }
}

if (!function_exists('check_walikelas')) {
    function check_walikelas()
    {
        $query = DB::table("tm_walikelas")
            ->where(['id_guru' => session()->get('sess_user')['id']])
            ->first();
        return @$query ? true : false;
    }
}

if (!function_exists('encrypt_openssl')) {
    function encrypt_openssl($data, $unique = false)
    {
        if ($unique) {
            $rand = rnd(5, true);
            $data = "$data%//%$rand";
        }

        $encryption_iv = '1234567891011121';
        $encryption_key = 'AWN51252';
        $ciphering = "AES-256-CBC";

        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;
        $encryption = openssl_encrypt(
            $data,
            $ciphering,
            $encryption_key,
            $options,
            $encryption_iv
        );
        return $encryption;
    }
}

if (!function_exists('decrypt_openssl')) {
    function decrypt_openssl($data, $unique = false)
    {
        $decryption_iv = '1234567891011121';

        $ciphering = "AES-256-CBC";
        $decryption_key = 'AWN51252';

        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;
        $decryption = openssl_decrypt(
            $data,
            $ciphering,
            $decryption_key,
            $options,
            $decryption_iv
        );
        if (!$unique) {
            return $decryption;
        }
        $decryption = explode('%//%', $decryption);

        return @$decryption[0] ? $decryption[0] : '';
    }
}
