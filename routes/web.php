<?php

use App\Http\Controllers\Absensi\Absensi;
use App\Http\Controllers\Siswa\Siswa;
use App\Http\Controllers\Guru\Guru;
use App\Http\Controllers\Rekap\Rekap;
use App\Http\Controllers\Nilai\Nilai;
use App\Http\Controllers\Jadwal\Jadwal;
use App\Http\Controllers\Akademik\Akademik;
use App\Http\Controllers\Walikelas\Walikelas;
use App\Http\Controllers\Authorization;
use App\Http\Controllers\Dashboard\Dashboard;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Utilities
|--------------------------------------------------------------------------
*/

Route::get('login', [Authorization::class, 'signin'])->name('login');
Route::get('logout', [Authorization::class, 'signout']);
Route::get('/', [Dashboard::class, 'index']);
// APi call from AJAX Request
Route::post('api/check', [Authorization::class, 'check']);

/*
|--------------------------------------------------------------------------
| Akademik
|--------------------------------------------------------------------------
*/
Route::get('akademik/{type}', [Akademik::class, 'index']);
// APi call from AJAX Request
Route::get('akademik/api/{type}', [Akademik::class, 'get_akademik']);
Route::post('akademik/api/add/{type}', [Akademik::class, 'add']);
Route::post('akademik/api/edit/{type}', [Akademik::class, 'edit']);
Route::post('akademik/api/delete/{type}', [Akademik::class, 'delete']);

/*
|--------------------------------------------------------------------------
| Absensi
|--------------------------------------------------------------------------
*/
Route::get('absensi/persiswa', [Absensi::class, 'persiswa']);
Route::get('absensi/siswa', [Absensi::class, 'siswa']);
Route::get('absensi', [Absensi::class, 'index']);
// APi call from AJAX Request
Route::post('absensi/api/get/siswa', [Absensi::class, 'get_absensi_siswa']);
Route::post('absensi/api/get/persiswa', [Absensi::class, 'get_absensi_persiswa']);
Route::post('absensi/api/get/input_siswa', [Absensi::class, 'get_absensi_input']);
Route::post('absensi/api/post/input_siswa', [Absensi::class, 'post_absensi_input']);
Route::post('absensi/api/post/input_absensi', [Absensi::class, 'input_absensi']);

/*
|--------------------------------------------------------------------------
| Penilaian
|--------------------------------------------------------------------------
*/
Route::get('nilai/persiswa', [Nilai::class, 'persiswa']);
Route::get('nilai/siswa', [Nilai::class, 'siswa']);
Route::get('nilai', [Nilai::class, 'index']);
// APi call from AJAX Request
Route::post('nilai/api/get/siswa', [Nilai::class, 'get_nilai_siswa']);
Route::post('nilai/api/get/persiswa', [Nilai::class, 'get_nilai_persiswa']);
Route::post('nilai/api/update/siswa', [Nilai::class, 'update_nilai_siswa']);
Route::post('nilai/api/delete/siswa', [Nilai::class, 'delete_nilai_siswa']);

/*
|--------------------------------------------------------------------------
| Siswa
|--------------------------------------------------------------------------
*/
Route::get('siswa', [Siswa::class, 'index']);
// APi call from AJAX Request
Route::post('siswa/api/get/siswa', [Siswa::class, 'get_siswa']);
Route::post('siswa/api/post/insert_siswa', [Siswa::class, 'insert_siswa']);
Route::post('siswa/api/post/update_siswa', [Siswa::class, 'update_siswa']);
Route::post('siswa/api/post/delete_siswa', [Siswa::class, 'delete_siswa']);


/*
|--------------------------------------------------------------------------
| Jadwal
|--------------------------------------------------------------------------
*/
Route::get('jadwal', [Jadwal::class, 'index']);
// APi call from AJAX Request
Route::post('jadwal/api/get/jadwal', [Jadwal::class, 'get_jadwal']);

/*
|--------------------------------------------------------------------------
| Guru, Staff & Karyawan
|--------------------------------------------------------------------------
*/
Route::get('guru', [Guru::class, 'index']);
// APi call from AJAX Request
Route::post('guru/api/get/guru', [Guru::class, 'get_guru']);
Route::post('guru/api/post/insert_guru', [Guru::class, 'insert_guru']);
Route::post('guru/api/post/update_guru', [Guru::class, 'update_guru']);
Route::post('guru/api/post/delete_guru', [Guru::class, 'delete_guru']);

/*
|--------------------------------------------------------------------------
| Walikelas
|--------------------------------------------------------------------------
*/
Route::get('walikelas', [Walikelas::class, 'index']);
// APi call from AJAX Request
Route::post('walikelas/api/get/walikelas', [Walikelas::class, 'get_walikelas']);
Route::post('walikelas/api/post/insert_walikelas', [Walikelas::class, 'insert_walikelas']);
Route::post('walikelas/api/post/update_walikelas', [Walikelas::class, 'update_walikelas']);
Route::post('walikelas/api/post/delete_walikelas', [Walikelas::class, 'delete_walikelas']);

/*
|--------------------------------------------------------------------------
| Rekap
|--------------------------------------------------------------------------
*/
Route::get('rekap/raport', [Rekap::class, 'raport']);
// APi call from AJAX Request
Route::post('rekap/api/get/nilai', [Rekap::class, 'get_nilai_rekap']);
Route::post('rekap/api/get/siswa', [Rekap::class, 'get_siswa_rekap']);

Route::get('rekap/api/cetak/raport/{id}', [Rekap::class, 'cetak_raport']);
Route::get('rekap/api/cetak/absensi/{id}', [Rekap::class, 'cetak_absensi']);
