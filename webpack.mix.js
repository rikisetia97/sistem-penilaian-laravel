const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/build/scripts/core.js', 'public/js/core.js')
//     .styles('resources/assets/build/styles/ltr-core.css', 'public/css/ltr-core.css')
//     .copyDirectory('resources/assets/images', 'public/images')
//    .version();