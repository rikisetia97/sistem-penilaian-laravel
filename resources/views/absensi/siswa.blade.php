@extends('templates/main')
@section('title', 'Data Absensi Siswa | Admin')
@php $breadcrumb = ['title' => 'Data Absensi Siswa', 'icon' => 'fa fa-laptop']; @endphp
@section('md-breadcrumb') @php echo breadcrumb_desktop($breadcrumb) @endphp @endsection
@section('sm-breadcrumb') @php echo breadcrumb_mobile($breadcrumb) @endphp @endsection
@section('style')
<!-- Custom Style Here -->
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="portlet">
                <div class="portlet-header portlet-header-bordered">
                    <h3 class="portlet-title">Semua data absensi siswa</h3>
                    <div class="header-wrap">
                        <button class="btn btn-label-dark ml-2 trigger-modal-filter" data-toggle="tooltip" title="" data-placement="left" data-original-title="Pilih Opsi"><i class="fa fa-cog"></i> Pilih Opsi</button>
                    </div>
                    <div class="portlet-addon"><button class="btn btn-label-dark btn-icon" data-toggle="portlet" data-target="parent" data-behavior="toggleCollapse"><i class="fa fa-angle-down"></i></button></div>
                </div>
                <div class="portlet-body div-pre">
                    <i class="fa fa-info-circle"></i> Harap pilih opsi untuk menampilkan data absensi
                </div>
                <div class="div-data d-none">
                    <div class="portlet-body">
                        <table id="datatable-1" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="0">No.</th>
                                    <th>NISN</th>
                                    <th>Nama</th>
                                    <th>Kelas</th>
                                    <th>Mapel</th>
                                    <th>Th. Ajaran</th>
                                    <th width="0" style="background-color:#99db9e;">Hadir</th>
                                    <th width="0" style="background-color:#ff9e9e;">Alpha</th>
                                    <th width="0" style="background-color:#ffe89a;">Izin</th>
                                    <th width="0" style="background-color: #8ed6eb;">Sakit</th>
                                    <th width="0">Total</th>
                                </tr>
                            </thead>
                            <tbody class="appendData">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal filter -->
<div class="modal fade modal-filter" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Filter Data Berdasarkan</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="siswa" class="col-sm-3 col-form-label col-form-label">Kelas</label>
                    <div class="col-sm-9">
                        <select class="form-control kelas_select2">
                            <option></option>
                            @foreach($kelas as $item)
                            <option value="{{$item->id}}">{{$item->kelas}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="siswa" class="col-sm-3 col-form-label col-form-label">Jurusan</label>
                    <div class="col-sm-9">
                        <select class="form-control jurusan_select2">
                            <option></option>
                            @foreach($jurusan as $item)
                            <option value="{{$item->id}}">{{$item->jurusan}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="siswa" class="col-sm-3 col-form-label col-form-label">Semester</label>
                    <div class="col-sm-9">
                        <select class="form-control semester_select2">
                            <option></option>
                            @foreach($semester as $item)
                            <option value="{{$item->id}}">{{$item->semester}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="siswa" class="col-sm-3 col-form-label col-form-label">Pelajaran</label>
                    <div class="col-sm-4">
                        <select class="form-control pelajaran_select2">
                            <option></option>
                            @foreach($pelajaran as $item)
                            <option value="{{$item->id}}">{{$item->pelajaran}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label for="siswa" class="col-sm-2 col-form-label col-form-label">Th. Ajaran</label>
                    <div class="col-sm-3">
                        <select class="form-control tahun_ajaran_select2">
                            <option></option>
                            @foreach($tahun_ajaran as $item)
                            <option value="{{$item->id}}">{{$item->tahun_ajaran}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2 btn-filter"><i class="fa fa-filter"></i> Filter </button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<!-- Custom Script Here -->
<script>
    var id_siswa,
        id_kelas,
        id_pelajaran,
        id_jurusan,
        id_semester,
        id_tahun_ajaran;

    function get_data() {
        $('.div-pre').addClass('d-none');
        $('.div-data').removeClass('d-none');
        if (id_siswa != undefined || id_kelas != undefined || id_pelajaran != undefined || id_tahun_ajaran != undefined) {
            $('.trigger-reset-filter').removeAttr('style');
        } else {
            $('.trigger-reset-filter').attr('style', 'display:none');
        }
        const table = $("#datatable-1");
        table.DataTable().clear();
        table.DataTable().destroy();
        $.ajax({
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id_siswa: id_siswa,
                id_kelas: id_kelas,
                id_pelajaran: id_pelajaran,
                id_tahun_ajaran: id_tahun_ajaran,
                id_jurusan: id_jurusan,
                id_semester: id_semester,
            },
            cache: false,
            url: "../absensi/api/get/siswa",
            success: function(response) {
                var obj = JSON.parse(response);
                no = 1;
                obj.forEach(item => {
                    $('.appendData').append(`
                                <tr>
                                    <td>` + no + `</td>
                                    <td>` + item['nisn'] + `</td>
                                    <td>` + item['nama'] + `</td>
                                    <td>` + item['kelas'] + `</td>
                                    <td>` + item['pelajaran'] + `</td>
                                    <td>` + item['tahun_ajaran'] + `</td>
                                    <td class="font-weight-bold" style="background-color:#88bc8c42;">` + item['hadir'] + `</td>
                                    <td class="font-weight-bold" style="background-color:#edc8c8;">` + item['alpha'] + `</td>
                                    <td class="font-weight-bold" style="background-color:#efe7c9;">` + item['ijin'] + `</td>
                                    <td class="font-weight-bold" style="background-color:#e1f4fa;">` + item['ijin'] + `</td>
                                    <td class="font-weight-bold">` + (parseInt(item['hadir']) + parseInt(item['ijin']) + parseInt(item['sakit']) + parseInt(item['alpha'])) + `</td>
                                </tr>
                    `);
                    no++
                });
                var e = (1025 <= $(window).width() ? $("#sticky-header-desktop") : $("#sticky-header-mobile")).height();
                table.DataTable({
                    select: true,
                    lengthMenu: [
                        [-1, 500, 250, 100, 50, 10, 5],
                        ["All", 500, 250, 100, 50, 10, 5]
                    ],
                    scrollCollapse: !0,
                    scrollY: "50vh",
                    scrollX: !0,
                    fixedHeader: {
                        header: !0,
                        headerOffset: e
                    },
                    dom: "\n      <'row'<'col-sm-6 text-center text-sm-".concat(t ? "left" : "left", "'B><'col-sm-6 text-center text-sm-").concat(t ? "left" : "right", " mt-2 mt-sm-0'f>>\n      <'row'<'col-12'tr>>\n      <'row align-items-baseline'<'col-md-5'i><'col-md-2 mt-2 mt-md-0'l><'col-md-5'p>>\n    "),
                    buttons: ["print"]
                });
            }
        });
    };

    $('.trigger-modal-filter').click(function() {
        $('.modal-filter').modal('show')
    });

    $('.siswa_select2').select2({
        placeholder: 'Pilih siswa'
    });
    $('.kelas_select2').select2({
        placeholder: 'Pilih kelas'
    });
    $('.pelajaran_select2').select2({
        placeholder: 'Pilih pelajaran'
    });
    $('.tahun_ajaran_select2').select2({
        placeholder: 'Pilih tahun'
    });
    $('.jurusan_select2').select2({
        placeholder: 'Pilih jurusan'
    });
    $('.semester_select2').select2({
        placeholder: 'Pilih semester'
    });

    $('.btn-filter').click(function() {
        id_siswa = $('.siswa_select2').val();
        id_kelas = $('.kelas_select2').val();
        id_pelajaran = $('.pelajaran_select2').val();
        id_tahun_ajaran = $('.tahun_ajaran_select2').val();
        id_jurusan = $('.jurusan_select2').val();
        id_semester = $('.semester_select2').val();
        $('.modal-filter').modal('hide');
        get_data();
    });
    $('.trigger-reset-filter').click(function() {
        id_siswa = null;
        id_kelas = null;
        id_pelajaran = null;
        id_tahun_ajaran = null;
        id_jurusan = null;
        id_semester = null;
        get_data();
    })
</script>
@endsection