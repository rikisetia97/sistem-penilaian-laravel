@extends('templates/main')
@section('title', 'Data Siswa | Admin')
@php $breadcrumb = ['title' => 'Data Siswa', 'icon' => 'fa fa-laptop']; @endphp
@section('md-breadcrumb') @php echo breadcrumb_desktop($breadcrumb) @endphp @endsection
@section('sm-breadcrumb') @php echo breadcrumb_mobile($breadcrumb) @endphp @endsection
@section('style')
<!-- Custom Style Here -->
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="portlet">
                <div class="portlet-header portlet-header-bordered">
                    <h3 class="portlet-title">Semua data siswa</h3>
                    <button class="btn btn-success ml-2 trigger-modal-filter" data-toggle="tooltip" title="" data-placement="left" data-original-title="Tambah Siswa Baru"><i class="fa fa-plus"></i> Tambah Siswa Baru</button>
                    <div class="header-wrap">
                        <button class="btn btn-label-dark ml-2 trigger-reset-filter" data-toggle="tooltip" title="" data-placement="left" data-original-title="Reset Filter Data"><i class="fa fa-backspace"></i> Reset Filter</button>
                        <button class="btn btn-label-dark ml-2 trigger-modal-filter" data-toggle="tooltip" title="" data-placement="left" data-original-title="Filter Data Absensi"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                    <div class="portlet-addon"><button class="btn btn-label-dark btn-icon" data-toggle="portlet" data-target="parent" data-behavior="toggleCollapse"><i class="fa fa-angle-down"></i></button></div>
                </div>

                <div class="portlet-body">
                    <table id="datatable-1" class="table table-bordered table-striped table-hover nowrap">
                        <thead>
                            <tr>
                                <th width="0">No.</th>
                                <th>NISN</th>
                                <th>Nama</th>
                                <th>JK</th>
                                <th>Jurusan</th>
                                <th>Kelas</th>
                                <th>Alamat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="appendData">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<!-- Custom Script Here -->
<script>
    var id_kelas,
        id_jurusan;
    get_data();

    function get_data() {
        if (id_kelas != undefined || id_jurusan != undefined) {
            $('.trigger-reset-filter').removeAttr('style');
        } else {
            $('.trigger-reset-filter').attr('style', 'display:none');
        }
        const table = $("#datatable-1");
        table.DataTable().clear();
        table.DataTable().destroy();
        $.ajax({
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id_kelas: id_kelas,
                id_jurusan: id_jurusan,
            },
            cache: false,
            url: "../siswa/api/get/siswa",
            success: function(response) {
                var obj = JSON.parse(response);
                no = 1;
                obj.forEach(item => {
                    $('.appendData').append(`
                                <tr>
                                    <td>` + no + `</td>
                                    <td>` + item['nisn'] + `</td>
                                    <td>` + item['nama'] + `</td>
                                    <td>` + item['gender'] + `</td>
                                    <td>` + item['jurusan'] + `</td>
                                    <td>` + item['kelas'] + `</td>
                                    <td>` + item['alamat'] + `</td>
                                    <td>
                                    <a href="#" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                    `);
                    no++
                });
                var e = (1025 <= $(window).width() ? $("#sticky-header-desktop") : $("#sticky-header-mobile")).height();
                table.DataTable({
                    select: true,
                    lengthMenu: [
                        [-1, 500, 250, 100, 50, 10, 5],
                        ["All", 500, 250, 100, 50, 10, 5]
                    ],
                    scrollCollapse: !0,
                    scrollY: "50vh",
                    scrollX: !0,
                    fixedHeader: {
                        header: !0,
                        headerOffset: e
                    },
                    dom: "\n      <'row'<'col-sm-6 text-center text-sm-".concat(t ? "left" : "left", "'B><'col-sm-6 text-center text-sm-").concat(t ? "left" : "right", " mt-2 mt-sm-0'f>>\n      <'row'<'col-12'tr>>\n      <'row align-items-baseline'<'col-md-5'i><'col-md-2 mt-2 mt-md-0'l><'col-md-5'p>>\n    "),
                    buttons: ["print"]
                });
            }
        });
    };

    $('.trigger-modal-filter').click(function() {
        $('.modal-filter').modal('show')
    });

    $('.kelas_select2').select2({
        placeholder: 'Pilih kelas'
    });
    $('.jurusan_select2').select2({
        placeholder: 'Pilih jurusan'
    });

    $('.btn-filter').click(function() {
        id_kelas = $('.kelas_select2').val();
        id_jurusan = $('.jurusan_select2').val();
        $('.modal-filter').modal('hide');
        get_data();
    });
    $('.trigger-reset-filter').click(function() {
        id_kelas = null;
        id_jurusan = null;
        get_data();
    })
</script>
@endsection