@extends('templates/main')
@section('title', 'Dashboard | Admin')
@php $breadcrumb = ['title' => 'Dashboard', 'icon' => 'fa fa-tachometer-alt']; @endphp
@section('md-breadcrumb') @php echo breadcrumb_desktop($breadcrumb) @endphp @endsection
@section('sm-breadcrumb') @php echo breadcrumb_mobile($breadcrumb) @endphp @endsection
@section('style')
<!-- Custom Style Here -->
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="portlet">
                <h5 class="p-3">Selamat datang di SMK Karangdapdap 😄</h3>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- Custom Script Here -->
@endsection