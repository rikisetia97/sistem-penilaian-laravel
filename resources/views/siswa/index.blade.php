@extends('templates/main')
@section('title', 'Data Siswa | Admin')
@php $breadcrumb = ['title' => 'Data Siswa', 'icon' => 'fa fa-laptop']; @endphp
@section('md-breadcrumb') @php echo breadcrumb_desktop($breadcrumb) @endphp @endsection
@section('sm-breadcrumb') @php echo breadcrumb_mobile($breadcrumb) @endphp @endsection
@section('style')
<!-- Custom Style Here -->
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="portlet">
                <div class="portlet-header portlet-header-bordered">
                    <h3 class="portlet-title">Semua data siswa</h3>
                    <button class="btn btn-label-dark ml-2 trigger-modal-filter" data-toggle="tooltip" title="" data-placement="left" data-original-title="Tambah Siswa Baru"><i class="fa fa-plus"></i> Tambah Siswa Baru</button>
                    <div class="portlet-addon"><button class="btn btn-label-dark btn-icon" data-toggle="portlet" data-target="parent" data-behavior="toggleCollapse"><i class="fa fa-angle-down"></i></button></div>
                </div>

                <div class="portlet-body">
                    <table id="datatable-1" class="table table-bordered table-striped table-hover nowrap">
                        <thead>
                            <tr>
                                <th width="0">No.</th>
                                <th>NISN</th>
                                <th>Kelas</th>
                                <th>Jurusan</th>
                                <th>Nama</th>
                                <th>JK</th>
                                <th>Tgl. Lahir</th>
                                <th>Alamat</th>
                                <th>Tgl. Bergabung</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="appendData">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal filter -->
<div class="modal fade modal-filter" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Guru</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="alert alert-label-success mb-2">
                    <div class="alert-icon"><i class="fa fa-info-circle"></i></div>
                    <div class="alert-content"> Kata sandi berdasarkan tanggal lahir, cth : 1997/12/08 (dd/mm/dd), maka passwordnya adalah 08121997 (dd/mm/yy) </div>
                </div>

                <div class="form-group mt-2"><label for="nama">Nama Lengkap</label>
                    <input type="text" class="form-control input-nama">
                    <small class="form-text text-muted">Mohon masukkan nama lengkap</small>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group mt-2"><label for="nama">NISN</label>
                            <input type="number" class="form-control input-nisn">
                            <small class="form-text text-muted">Mohon masukkan NIS/NISN</small>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group mt-2"><label for="nama">No. HP</label>
                            <input type="number" class="form-control input-nomor_hp" placeholder="+62...">
                            <small class="form-text text-muted">Mohon masukkan nomor hp</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group mt-2"><label for="nama">Jenis Kelamin</label>
                            <select class="form-control input-gender">
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                            <small class="form-text text-muted">Silahkan pilih jenis kelamin</small>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group mt-2"><label for="nama">Tgl. Lahir</label>
                            <input type="date" class="form-control input-tgl_lahir">
                            <small class="form-text text-muted">Mohon masukkan tanggal lahir</small>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-2"><label for="nama">Alamat</label>
                    <textarea class="form-control input-alamat" cols="3" rows="3"></textarea>
                    <small class="form-text text-muted">Mohon masukkan alamat lengkap</small>
                </div>

                <div class="form-group mt-2"><label for="nama">Kelas</label>
                    <select class="form-control kelas_select2">
                        <option></option>
                        @foreach($kelas as $item)
                        <option value="{{$item->id}}">{{$item->kelas}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih kelas</small>
                </div>
                <div class="form-group mt-2"><label for="nama">Jurusan</label>
                    <select class="form-control jurusan_select2">
                        <option></option>
                        @foreach($jurusan as $item)
                        <option value="{{$item->id}}">{{$item->jurusan}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih jurusan</small>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2 btn-save"><i class="fa fa-save"></i> Simpan </button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-edit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Guru</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <input type="hidden" class="edit-id">
                <div class="form-group mt-2"><label for="nama">Nama Lengkap</label>
                    <input type="text" class="form-control edit-nama">
                    <small class="form-text text-muted">Mohon masukkan nama lengkap</small>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group mt-2"><label for="nama">NISN</label>
                            <input type="number" class="form-control edit-nisn">
                            <small class="form-text text-muted">Mohon masukkan NIS/NISN</small>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group mt-2"><label for="nama">No. HP</label>
                            <input type="number" class="form-control edit-nomor_hp" placeholder="+62...">
                            <small class="form-text text-muted">Mohon masukkan nomor hp</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group mt-2"><label for="nama">Jenis Kelamin</label>
                            <select class="form-control edit-gender">
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                            <small class="form-text text-muted">Silahkan pilih jenis kelamin</small>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group mt-2"><label for="nama">Tgl. Lahir</label>
                            <input type="date" class="form-control edit-tgl_lahir">
                            <small class="form-text text-muted">Mohon masukkan tanggal lahir</small>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-2"><label for="nama">Alamat</label>
                    <textarea class="form-control edit-alamat" cols="3" rows="3"></textarea>
                    <small class="form-text text-muted">Mohon masukkan alamat lengkap</small>
                </div>

                <div class="form-group mt-2"><label for="nama">Kelas</label>
                    <select class="form-control edit-kelas_select2">
                        <option></option>
                        @foreach($kelas as $item)
                        <option value="{{$item->id}}">{{$item->kelas}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih kelas</small>
                </div>
                <div class="form-group mt-2"><label for="nama">Jurusan</label>
                    <select class="form-control edit-jurusan_select2">
                        <option></option>
                        @foreach($jurusan as $item)
                        <option value="{{$item->id}}">{{$item->jurusan}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih jurusan</small>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2 btn-update-siswa"><i class="fa fa-save"></i> Simpan </button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal atur kkm -->
<div class="modal fade modal-selesai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Perhatian</h5>
            </div>
            <div class="modal-body">
                Data siswa berhasil ditambahkan!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal atur kkm -->
<div class="modal fade modal-selesai-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Perhatian</h5>
            </div>
            <div class="modal-body">
                Data siswa berhasil diperbaharui!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<!-- Custom Script Here -->
<script>
    var id_kelas,
        id_jurusan;
    get_data();

    function get_data() {
        if (id_kelas != undefined || id_jurusan != undefined) {
            $('.trigger-reset-filter').removeAttr('style');
        } else {
            $('.trigger-reset-filter').attr('style', 'display:none');
        }
        const table = $("#datatable-1");
        table.DataTable().clear();
        table.DataTable().destroy();
        $.ajax({
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id_kelas: id_kelas,
                id_jurusan: id_jurusan,
            },
            cache: false,
            url: "../siswa/api/get/siswa",
            success: function(response) {
                var obj = JSON.parse(response);
                no = 1;
                obj.forEach(item => {
                    $('.appendData').append(`
                                <tr>
                                    <td>` + no + `</td>
                                    <td>` + item['nisn'] + `</td>
                                    <td>` + item['kelas'] + `</td>
                                    <td>` + item['jurusan'] + `</td>
                                    <td>` + item['nama'] + `</td>
                                    <td>` + item['gender'] + `</td>
                                    <td>` + item['tgl_lahir'] + `</td>
                                    <td>` + item['alamat'] + `</td>
                                    <td>` + item['created_at'] + `</td>
                                    <td>
                                    <a href="javascript:void(0)" onclick='edit(` + JSON.stringify(item) + `)' class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void(0)" onclick="delete_siswa(` + item['id'] + `)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                    `);
                    no++
                });
                var e = (1025 <= $(window).width() ? $("#sticky-header-desktop") : $("#sticky-header-mobile")).height();
                table.DataTable({
                    select: true,
                    lengthMenu: [
                        [-1, 500, 250, 100, 50, 10, 5],
                        ["All", 500, 250, 100, 50, 10, 5]
                    ],
                    scrollCollapse: !0,
                    scrollY: "50vh",
                    scrollX: !0,
                    fixedHeader: {
                        header: !0,
                        headerOffset: e
                    },
                    dom: "\n      <'row'<'col-sm-6 text-center text-sm-".concat(t ? "left" : "left", "'B><'col-sm-6 text-center text-sm-").concat(t ? "left" : "right", " mt-2 mt-sm-0'f>>\n      <'row'<'col-12'tr>>\n      <'row align-items-baseline'<'col-md-5'i><'col-md-2 mt-2 mt-md-0'l><'col-md-5'p>>\n    "),
                    buttons: ["print"]
                });
            }
        });
    };

    $('.trigger-modal-filter').click(function() {
        $('.modal-filter').modal('show')
    });

    $('.kelas_select2').select2({
        placeholder: 'Pilih kelas'
    });
    $('.jurusan_select2').select2({
        placeholder: 'Pilih jurusan'
    });

    $('.edit-kelas_select2').select2({
        placeholder: 'Pilih kelas'
    });
    $('.edit-jurusan_select2').select2({
        placeholder: 'Pilih jurusan'
    });

    $('.btn-filter').click(function() {
        id_kelas = $('.kelas_select2').val();
        id_jurusan = $('.jurusan_select2').val();
        $('.modal-filter').modal('hide');
        get_data();
    });
    $('.trigger-reset-filter').click(function() {
        id_kelas = null;
        id_jurusan = null;
        get_data();
    });
    $('.btn-save').click(function() {
        $.ajax({
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                nisn: $('.input-nisn').val(),
                nama: $('.input-nama').val(),
                gender: $('.input-gender').val(),
                alamat: $('.input-alamat').val(),
                tgl_lahir: $('.input-tgl_lahir').val(),
                nomor_hp: $('.input-nomor_hp').val(),
                id_kelas: $('.kelas_select2').val(),
                id_jurusan: $('.jurusan_select2').val(),
            },
            cache: false,
            url: "../siswa/api/post/insert_siswa",
            success: function(response) {
                if (response) {
                    $('.modal-filter').modal('hide');
                    get_data();
                    setTimeout(() => {
                        $('.modal-selesai').modal('show');
                    }, 100);
                }
            }
        });
    });

    function edit(data) {
        $('.edit-id').val(data.id);
        $('.edit-nisn').val(data.nisn);
        $('.edit-nama').val(data.nama);
        $('.edit-gender').val(data.gender);
        $('.edit-alamat').val(data.alamat);
        $('.edit-tgl_lahir').val(data.tgl_lahir);
        $('.edit-nomor_hp').val(data.nomor_hp);
        $('.modal-edit').modal('show');
    };

    function delete_siswa(id) {
        alert(id);
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $.ajax({
                method: 'POST',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    id: id,
                },
                cache: false,
                url: "../siswa/api/post/delete_siswa",
                success: function(response) {
                    get_data();
                }
            });
        }
    };
    $('.btn-update-siswa').click(function() {
        $.ajax({
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id: $('.edit-id').val(),
                nisn: $('.edit-nisn').val(),
                nama: $('.edit-nama').val(),
                gender: $('.edit-gender').val(),
                alamat: $('.edit-alamat').val(),
                tgl_lahir: $('.edit-tgl_lahir').val(),
                nomor_hp: $('.edit-nomor_hp').val(),
                id_kelas: $('.edit-kelas_select2').val(),
                id_jurusan: $('.edit-jurusan_select2').val(),
            },
            cache: false,
            url: "../siswa/api/post/update_siswa",
            success: function(response) {
                if (response) {
                    $('.modal-edit').modal('hide');
                    get_data();
                    setTimeout(() => {
                        $('.modal-selesai-update').modal('show');
                    }, 100);
                }
            },
        });
    })
</script>
@endsection