@extends('templates/main')
@section('title', 'Data '.ucwords(str_replace('_',' ', $type)).' | Admin')
@php $breadcrumb = ['title' => 'Data '.ucwords(str_replace('_',' ', $type)), 'icon' => 'fa fa-university']; @endphp
@section('md-breadcrumb') @php echo breadcrumb_desktop($breadcrumb) @endphp @endsection
@section('sm-breadcrumb') @php echo breadcrumb_mobile($breadcrumb) @endphp @endsection
@section('style')
<!-- Custom Style Here -->
@endsection
@section('content')
@php $typeHtml = ucwords(str_replace('_',' ', $type)); @endphp
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="portlet">
                <div class="portlet-header portlet-header-bordered">
                    <h3 class="portlet-title">Data {{$typeHtml}} </h3>
                    <div class="header-wrap">
                        <button class="btn btn-label-dark ml-2 trigger-modal-add" data-toggle="tooltip" title="" data-placement="left" data-original-title="Tambah {{$typeHtml}}"><i class="fa fa-plus"></i> Tambah {{$typeHtml}}</button>
                    </div>
                    <div class="portlet-addon"><button class="btn btn-label-dark btn-icon" data-toggle="portlet" data-target="parent" data-behavior="toggleCollapse"><i class="fa fa-angle-down"></i></button></div>
                </div>
                <!-- <span class="text-muted my-2 text-center"><i class="far fa-question-circle"></i> Right click open context menu</span> -->

                <div class="portlet-body">
                    <table id="datatable-1" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>{{$typeHtml}}</th>
                                <th>Tanggal Dibuat</th>
                                <th>Tanggal Diperbaharui</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="appendData">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Akademik -->
<div class="modal fade modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah {{$typeHtml}}</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="form-group mt-2"><label for="nama">@if($type != 'tahun_ajaran') Nama @endif{{$typeHtml}}</label> <input type="text" class="form-control input-akademik"> <small class="form-text text-muted">Mohon masukkan @if($type != 'tahun_ajaran') nama @endif {{str_replace('_',' ', $type)}}</small></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2 btn-submit-add">Submit</button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit {{$typeHtml}}</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <input type="hidden" class="edit-akademik-id">
                <div class="form-group mt-2"><label for="nama">@if($type != 'tahun_ajaran') Nama @endif{{$typeHtml}}</label> <input type="text" class="form-control edit-akademik"> <small class="form-text text-muted">Mohon masukkan {{$typeHtml}}</small></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2 btn-submit-edit">Submit</button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus {{$typeHtml}}</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <input type="hidden" class="delete-akademik-id"> <span>Apakah anda yakin akan menghapus <span class="font-weight-bold data-name-akademik"></span>?</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger mr-2 btn-submit-delete">Delete</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="type" value="{{ $type }}">

@endsection
@section('script')
<!-- Custom Script Here -->
<script>
    var type = $('.type').val();
    get_data(type);

    function edit_item(type, id) {
        $('.modal-edit').modal('show');
        $('.edit-akademik').val(type);
        $('.edit-akademik-id').val(id);
    }

    function delete_item(type, id) {
        $('.modal-delete').modal('show');
        $('.delete-akademik-id').val(id);
        $('.data-name-akademik').html('"' + type + '"');
    }

    function get_data(type) {
        const table = $("#datatable-1");
        table.DataTable().clear();
        table.DataTable().destroy();
        $.ajax({
            type: "GET",
            url: "api/" + type,
            success: function(response) {
                var obj = JSON.parse(response);
                var no = 1;
                obj.forEach(item => {
                    $('.appendData').append(`
                        <tr class="context-menu-` + item['id'] + `">
                            <td>` + no + `</td>
                            <td>` + item[type] + `</td>
                            <td>` + item['created_at'] + `</td>
                            <td>` + (item['updated_at'] ? item['updated_at'] : '-') + `</td>
                            <td> 
                            <a href="javascript:void(0)" onclick="edit_item('` + item[type] + `',` + item['id'] + `)" class="btn btn-primary"><i class="fa fa-edit"></i></a> 
                            <a href="javascript:void(0)" onclick="delete_item('` + item[type] + `',` + item['id'] + `)" class="btn btn-danger"><i class="fa fa-trash"></i></a> 
                            </td>
                        </tr>
                    `);
                    $.contextMenu({
                        selector: ".context-menu-" + item['id'],
                        position: function(e, t, n) {
                            var o = t;
                            e.$menu.css({
                                top: n,
                                left: o
                            });
                        },
                        callback: function(e, t) {
                            if (e == 'edit') {
                                $('.modal-edit').modal('show');
                                $('.edit-akademik').val(item[type]);
                                $('.edit-akademik-id').val(item['id']);
                            } else if (e == 'delete') {
                                $('.modal-delete').modal('show');
                                $('.delete-akademik-id').val(item['id']);
                                $('.data-name-akademik').html('"' + item[type] + '"');
                            }
                        },
                        items: {
                            edit: {
                                name: "Edit",
                                icon: "edit"
                            },
                            delete: {
                                name: "Hapus",
                                icon: "delete"
                            },
                        },
                    });
                    no++;
                });
                var e = (1025 <= $(window).width() ? $("#sticky-header-desktop") : $("#sticky-header-mobile")).height();
                table.DataTable({
                    select: true,
                    scrollCollapse: !0,
                    scrollY: "50vh",
                    scrollX: !0,
                    fixedHeader: {
                        header: !0,
                        headerOffset: e
                    },
                    dom: "\n      <'row'<'col-sm-6 text-center text-sm-".concat(t ? "left" : "left", "'B><'col-sm-6 text-center text-sm-").concat(t ? "left" : "right", " mt-2 mt-sm-0'f>>\n      <'row'<'col-12'tr>>\n      <'row align-items-baseline'<'col-md-5'i><'col-md-2 mt-2 mt-md-0'l><'col-md-5'p>>\n    "),
                    buttons: ["print"],
                })
            }
        });
    }

    $('.btn-submit-add').click(function() {
        var dataPost = {
            _token: $('meta[name="csrf-token"]').attr('content'),
            input_akademik: $('.input-akademik').val()
        };
        if (dataPost.input_akademik == '') {
            $('.input-akademik').addClass('is-invalid');
        } else {
            $.ajax({
                type: "POST",
                url: "api/add/" + type,
                data: dataPost,
                cache: false,
                success: function(response) {
                    $('.modal-add').modal('hide');
                    if (response) {
                        get_data(type);
                        $('.text-alert').html('Data berhasil ditambahkan!');
                        $('.modal-alert').modal('show');
                    } else {
                        $('.text-alert').html('Data gagal ditambahkan!');
                        $('.modal-alert').modal('show');
                    }
                }
            });
        }
    });
    $('.btn-submit-edit').click(function() {
        var dataPost = {
            _token: $('meta[name="csrf-token"]').attr('content'),
            id: $('.edit-akademik-id').val(),
            input_akademik: $('.edit-akademik').val()
        };
        if (dataPost.input_akademik == '') {
            $('.edit-akademik').addClass('is-invalid');
        } else {
            $.ajax({
                type: "POST",
                url: "api/edit/" + type,
                data: dataPost,
                cache: false,
                success: function(response) {
                    $('.modal-edit').modal('hide');
                    if (response) {
                        get_data(type);
                        $('.text-alert').html('Data berhasil diperbaharui!');
                        $('.modal-alert').modal('show');
                    } else {
                        $('.text-alert').html('Data gagal diperbaharui!');
                        $('.modal-alert').modal('show');
                    }
                }
            });
        }
    });
    $('.btn-submit-delete').click(function() {
        var dataPost = {
            _token: $('meta[name="csrf-token"]').attr('content'),
            id: $('.delete-akademik-id').val(),
        };
        $.ajax({
            type: "POST",
            url: "api/delete/" + type,
            data: dataPost,
            cache: false,
            success: function(response) {
                $('.modal-delete').modal('hide');
                if (response) {
                    get_data(type);
                    $('.text-alert').html('Data berhasil dihapus!');
                    $('.modal-alert').modal('show');
                } else {
                    $('.text-alert').html('Data gagal dihapus!');
                    $('.modal-alert').modal('show');
                }
            }
        });
    });

    $(".input-akademik").keyup(function(e) {
        if (e.which === 13) {
            $(".btn-submit-add").click();
        }
    });
    $(".edit-akademik").keyup(function(e) {
        if (e.which === 13) {
            $(".btn-submit-edit").click();
        }
    });

    $('.trigger-modal-add').click(function() {
        $('.input-akademik').val('');
        $('.input-akademik').removeClass('is-invalid');
        $('.modal-add').modal('show')
    });

    $(".modal-add").on("shown.bs.modal", function() {
        $(".modal-body input").focus();
    });
    $(".modal-edit").on("shown.bs.modal", function() {
        $('.edit-akademik').removeClass('is-invalid');
        $(".modal-body input").focus();
    });
    $(".modal-alert").on("shown.bs.modal", function() {
        $(".modal-footer button").focus();
    });
    $(".modal-delete").on("shown.bs.modal", function() {
        $(".btn-submit-delete").focus();
    });
</script>
@endsection