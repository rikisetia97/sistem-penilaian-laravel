@extends('templates/main')
@section('title', 'Data Walikelas | Admin')
@php $breadcrumb = ['title' => 'Data Walikelas', 'icon' => 'fa fa-laptop']; @endphp
@section('md-breadcrumb') @php echo breadcrumb_desktop($breadcrumb) @endphp @endsection
@section('sm-breadcrumb') @php echo breadcrumb_mobile($breadcrumb) @endphp @endsection
@section('style')
<!-- Custom Style Here -->
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="portlet">
                <div class="portlet-header portlet-header-bordered">
                    <h3 class="portlet-title">Semua data walikelas</h3>
                    <button class="btn btn-label-dark ml-2 trigger-modal-filter" data-toggle="tooltip" title="" data-placement="left" data-original-title="Tambah Guru Baru"><i class="fa fa-plus"></i> Tambah Walikelas Baru</button>
                    <div class="portlet-addon"><button class="btn btn-label-dark btn-icon" data-toggle="portlet" data-target="parent" data-behavior="toggleCollapse"><i class="fa fa-angle-down"></i></button></div>
                </div>

                <div class="portlet-body">
                    <table id="datatable-1" class="table table-bordered table-striped table-hover nowrap">
                        <thead>
                            <tr>
                                <th width="0">No.</th>
                                <th>Kelas</th>
                                <th>Jurusan</th>
                                <th>NIP</th>
                                <th>Nama Guru</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="appendData">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal filter -->
<div class="modal fade modal-filter" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Walikelas</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="form-group mt-2"><label for="nama">Guru</label>
                    <select class="form-control guru_select2">
                        <option></option>
                        @foreach($guru as $item)
                        <option value="{{$item->id}}">{{$item->nip}} | {{$item->nama}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih guru</small>
                </div>
                <div class="form-group mt-2"><label for="nama">Kelas</label>
                    <select class="form-control kelas_select2">
                        <option></option>
                        @foreach($kelas as $item)
                        <option value="{{$item->id}}">{{$item->kelas}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih kelas</small>
                </div>
                <div class="form-group mt-2"><label for="nama">Jurusan</label>
                    <select class="form-control jurusan_select2">
                        <option></option>
                        @foreach($jurusan as $item)
                        <option value="{{$item->id}}">{{$item->jurusan}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih jurusan</small>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2 btn-save"><i class="fa fa-save"></i> Simpan </button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal filter -->
<div class="modal fade modal-edit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data Walikelas</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <input type="hidden" class="edit-id">
                <div class="form-group mt-2"><label for="nama">Guru</label>
                    <select class="form-control edit-guru_select2">
                        <option></option>
                        @foreach($guru as $item)
                        <option value="{{$item->id}}">{{$item->nip}} | {{$item->nama}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih guru</small>
                </div>
                <div class="form-group mt-2"><label for="nama">Kelas</label>
                    <select class="form-control edit-kelas_select2">
                        <option></option>
                        @foreach($kelas as $item)
                        <option value="{{$item->id}}">{{$item->kelas}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih kelas</small>
                </div>
                <div class="form-group mt-2"><label for="nama">Jurusan</label>
                    <select class="form-control edit-jurusan_select2">
                        <option></option>
                        @foreach($jurusan as $item)
                        <option value="{{$item->id}}">{{$item->jurusan}}</option>
                        @endforeach
                    </select>
                    <small class="form-text text-muted">Mohon pilih jurusan</small>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2 btn-update-walikelas"><i class="fa fa-save"></i> Simpan </button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal atur kkm -->
<div class="modal fade modal-selesai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Perhatian</h5>
            </div>
            <div class="modal-body">
                Data guru berhasil ditambahkan!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal atur kkm -->
<div class="modal fade modal-selesai-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Perhatian</h5>
            </div>
            <div class="modal-body">
                Data guru berhasil diperbaharui!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<!-- Custom Script Here -->
<script>
    var id_kelas,
        id_jurusan;
    get_data();

    function get_data() {
        if (id_kelas != undefined || id_jurusan != undefined) {
            $('.trigger-reset-filter').removeAttr('style');
        } else {
            $('.trigger-reset-filter').attr('style', 'display:none');
        }
        const table = $("#datatable-1");
        table.DataTable().clear();
        table.DataTable().destroy();
        $.ajax({
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id_kelas: id_kelas,
                id_jurusan: id_jurusan,
            },
            cache: false,
            url: "../walikelas/api/get/walikelas",
            success: function(response) {
                var obj = JSON.parse(response);
                no = 1;
                obj.forEach(item => {
                    $('.appendData').append(`
                                <tr>
                                    <td>` + no + `</td>
                                    <td>` + item['kelas'] + `</td>
                                    <td>` + item['jurusan'] + `</td>
                                    <td>` + item['nip'] + `</td>
                                    <td>` + item['nama'] + `</td>
                                    <td>
                                    <a href="javascript:void(0)" onclick='edit(` + JSON.stringify(item) + `)' class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void(0)" onclick="delete_walikelas(` + item['id'] + `)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                    `);
                    no++
                });
                var e = (1025 <= $(window).width() ? $("#sticky-header-desktop") : $("#sticky-header-mobile")).height();
                table.DataTable({
                    select: true,
                    lengthMenu: [
                        [-1, 500, 250, 100, 50, 10, 5],
                        ["All", 500, 250, 100, 50, 10, 5]
                    ],
                    scrollCollapse: !0,
                    scrollY: "50vh",
                    scrollX: !0,
                    fixedHeader: {
                        header: !0,
                        headerOffset: e
                    },
                    dom: "\n      <'row'<'col-sm-6 text-center text-sm-".concat(t ? "left" : "left", "'B><'col-sm-6 text-center text-sm-").concat(t ? "left" : "right", " mt-2 mt-sm-0'f>>\n      <'row'<'col-12'tr>>\n      <'row align-items-baseline'<'col-md-5'i><'col-md-2 mt-2 mt-md-0'l><'col-md-5'p>>\n    "),
                    buttons: ["print"]
                });
            }
        });
    };

    $('.trigger-modal-filter').click(function() {
        $('.modal-filter').modal('show')
    });

    $('.guru_select2').select2({
        placeholder: 'Pilih guru'
    });
    $('.kelas_select2').select2({
        placeholder: 'Pilih kelas'
    });
    $('.jurusan_select2').select2({
        placeholder: 'Pilih jurusan'
    });
    $('.edit-guru_select2').select2({
        placeholder: 'Pilih guru'
    });
    $('.edit-kelas_select2').select2({
        placeholder: 'Pilih kelas'
    });
    $('.edit-jurusan_select2').select2({
        placeholder: 'Pilih jurusan'
    });
    $('.btn-save').click(function() {
        $.ajax({
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id_guru: $('.guru_select2').val(),
                id_jurusan: $('.jurusan_select2').val(),
                id_kelas: $('.kelas_select2').val(),
            },
            cache: false,
            url: "../walikelas/api/post/insert_walikelas",
            success: function(response) {
                if (response) {
                    $('.modal-filter').modal('hide');
                    get_data();
                    setTimeout(() => {
                        $('.modal-selesai').modal('show');
                    }, 100);
                }
            }
        });
    });

    function edit(data) {
        $('.edit-id').val(data.id);
        $(".edit-kelas_select2").val(data.id_kelas).select2();
        $(".edit-jurusan_select2").val(data.id_jurusan).select2();
        $(".edit-guru_select2").val(data.id_guru).select2();
        $('.modal-edit').modal('show');
    };

    function delete_walikelas(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $.ajax({
                method: 'POST',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    id: id,
                },
                cache: false,
                url: "../walikelas/api/post/delete_walikelas",
                success: function(response) {
                    get_data();
                }
            });
        }
    };
    $('.btn-update-walikelas').click(function() {
        $.ajax({
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id: $('.edit-id').val(),
                id_guru: $('.edit-guru_select2').val(),
                id_jurusan: $('.edit-jurusan_select2').val(),
                id_kelas: $('.edit-kelas_select2').val(),
            },
            cache: false,
            url: "../walikelas/api/post/update_walikelas",
            success: function(response) {
                if (response) {
                    $('.modal-edit').modal('hide');
                    get_data();
                    setTimeout(() => {
                        $('.modal-selesai-update').modal('show');
                    }, 100);
                }
            }
        });
    })
</script>
@endsection