@extends('templates/main')
@section('title', 'Daftar Nilai Saya | Admin')
@php $breadcrumb = ['title' => 'Daftar Nilai Saya', 'icon' => 'fa fa-laptop']; @endphp
@section('md-breadcrumb') @php echo breadcrumb_desktop($breadcrumb) @endphp @endsection
@section('sm-breadcrumb') @php echo breadcrumb_mobile($breadcrumb) @endphp @endsection
@section('style')
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="portlet">
                <div class="portlet-header portlet-header-bordered">
                    <h3 class="portlet-title">Daftar Nilai Saya</h3>
                    <a href="{{url('rekap/api/cetak/raport/'.session()->get('sess_user')['id'])}}" class="btn btn-label-dark ml-2" title="" data-placement="left" data-original-title="Cetak Raport"><i class="fa fa-file-export"></i> Cetak Raport</a>
                    <button class="btn btn-label-dark ml-2 trigger-modal-filter" data-toggle="tooltip" title="" data-placement="left" data-original-title="Pilih Opsi"><i class="fa fa-cog"></i> Pilih Opsi</button>
                    <div class="portlet-addon"><button class="btn btn-label-dark btn-icon" data-toggle="portlet" data-target="parent" data-behavior="toggleCollapse"><i class="fa fa-angle-down"></i></button></div>
                </div>
                <div class="portlet-body div-pre">
                    Harap pilih opsi untuk menampilkan nilai sesuai kriteria yang dipilih
                </div>
                <div class="div-data d-none">
                    <div class="portlet-body">
                        <table id="datatable-1" class="table table-bordered table-striped table-hover nowrap">
                            <thead>
                                <tr>
                                    <th width="0">No.</th>
                                    <th width="100">NISN</th>
                                    <th>Nama</th>
                                    <th>Kelas</th>
                                    <th>Jurusan</th>
                                    <th>Semester</th>
                                    <th>Tahun Ajaran</th>
                                    <th width="0" style="background-color:#99db9e;">KKM</th>
                                    <th width="0" style="background-color: #8ed6eb;">Nilai</th>
                                </tr>
                            </thead>
                            <tbody class="appendData">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal filter -->
<div class="modal fade modal-filter" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Filter Data Berdasarkan</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">

                <div class="form-group row">
                    <label for="siswa" class="col-sm-3 col-form-label col-form-label">Kelas</label>
                    <div class="col-sm-9">
                        <select class="form-control kelas_select2">
                            <option></option>
                            @foreach($kelas as $item)
                            <option value="{{$item->id}}">{{$item->kelas}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="siswa" class="col-sm-3 col-form-label col-form-label">Semester</label>
                    <div class="col-sm-9">
                        <select class="form-control semester_select2">
                            <option></option>
                            @foreach($semester as $item)
                            <option value="{{$item->id}}">{{$item->semester}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="siswa" class="col-sm-3 col-form-label col-form-label">Pelajaran</label>
                    <div class="col-sm-4">
                        <select class="form-control pelajaran_select2">
                            <option></option>
                            @foreach($pelajaran as $item)
                            <option value="{{$item->id}}">{{$item->pelajaran}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label for="siswa" class="col-sm-2 col-form-label col-form-label">Th. Ajaran</label>
                    <div class="col-sm-3">
                        <select class="form-control tahun_ajaran_select2">
                            <option></option>
                            @foreach($tahun_ajaran as $item)
                            <option value="{{$item->id}}">{{$item->tahun_ajaran}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2 btn-filter"><i class="fa fa-filter"></i> Cek Nilai </button>
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit -->
<div class="modal fade modal_ed_nilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Nilai</h5>
            </div>
            <div class="modal-body">
                <input type="hidden" class="ed_id">
                <div class="form-group row mt-2">
                    <label for="colFormLabelNilai" class="col-sm-2 col-form-label col-form-label">Nilai</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control ed_nilai" id="colFormLabelNilai">
                    </div>
                </div>
            </div>
            <div class=" modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-simpan-perubahan">Simpan</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<!-- Custom Script Here -->
<script>
    var id_siswa,
        id_kelas,
        id_pelajaran,
        id_jurusan,
        id_semester,
        id_tahun_ajaran;
    get_data();

    function get_data() {
        $('.div-pre').addClass('d-none');
        $('.div-data').removeClass('d-none');
        if (id_siswa != undefined || id_kelas != undefined || id_pelajaran != undefined || id_tahun_ajaran != undefined) {
            $('.trigger-reset-filter').removeAttr('style');
        } else {
            $('.trigger-reset-filter').attr('style', 'display:none');
        }
        const table = $("#datatable-1");
        table.DataTable().clear();
        table.DataTable().destroy();
        $.ajax({
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id_siswa: id_siswa,
                id_kelas: id_kelas,
                id_pelajaran: id_pelajaran,
                id_tahun_ajaran: id_tahun_ajaran,
                id_jurusan: id_jurusan,
                id_semester: id_semester,
            },
            cache: false,
            url: "../nilai/api/get/persiswa",
            success: function(response) {
                var obj = JSON.parse(response);
                no = 1;
                if (obj.length > 0) {
                    $('.mapel').html(obj[0].pelajaran);
                    $('.kelas').html(obj[0].kelas);
                    $('.jurusan').html(obj[0].jurusan);
                    $('.semester').html(obj[0].semester);
                    $('.tahun_ajaran').html(obj[0].tahun_ajaran);
                    $('.kkm').html(obj[0].kkm ? obj[0].kkm : 0);
                } else {
                    $('.mapel').html($(".pelajaran_select2 option:selected").text());
                    $('.kelas').html($(".kelas_select2 option:selected").text());
                    $('.jurusan').html($(".jurusan_select2 option:selected").text());
                    $('.semester').html($(".semester_select2 option:selected").text());
                    $('.tahun_ajaran').html($(".tahun_ajaran_select2 option:selected").text());
                    $('.kkm').html(0);
                }
                obj.forEach(item => {
                    $('.appendData').append(`
                                <tr>
                                    <td>` + no + `</td>
                                    <td>` + item['nisn'] + `</td>
                                    <td>` + item['nama'] + `</td>
                                    <td>` + item['kelas'] + `</td>
                                    <td>` + item['jurusan'] + `</td>
                                    <td>` + item['semester'] + `</td>
                                    <td>` + item['tahun_ajaran'] + `</td>
                                    <td class="font-weight-bold" style="background-color:#88bc8c42;">` + item['kkm'] + `</td>
                                    <td class="font-weight-bold" style="background-color:#e1f4fa;">` + item['nilai'] + `</td>
                                </tr>
                    `);
                    no++
                });
                var e = (1025 <= $(window).width() ? $("#sticky-header-desktop") : $("#sticky-header-mobile")).height();
                table.DataTable({
                    select: true,
                    lengthMenu: [
                        [-1, 500, 250, 100, 50, 10, 5],
                        ["All", 500, 250, 100, 50, 10, 5]
                    ],
                    scrollCollapse: !0,
                    scrollY: "50vh",
                    scrollX: !0,
                    fixedHeader: {
                        header: !0,
                        headerOffset: e
                    },
                    dom: "\n      <'row'<'col-sm-6 text-center text-sm-".concat(t ? "left" : "left", "'B><'col-sm-6 text-center text-sm-").concat(t ? "left" : "right", " mt-2 mt-sm-0'f>>\n      <'row'<'col-12'tr>>\n      <'row align-items-baseline'<'col-md-5'i><'col-md-2 mt-2 mt-md-0'l><'col-md-5'p>>\n    "),
                    buttons: ["print"]
                });
            }
        });
    };

    $('.trigger-modal-filter').click(function() {
        $('.modal-filter').modal('show')
    });

    $('.siswa_select2').select2({
        placeholder: 'Pilih siswa'
    });
    $('.kelas_select2').select2({
        placeholder: 'Pilih kelas'
    });
    $('.pelajaran_select2').select2({
        placeholder: 'Pilih pelajaran'
    });
    $('.tahun_ajaran_select2').select2({
        placeholder: 'Pilih tahun'
    });
    $('.jurusan_select2').select2({
        placeholder: 'Pilih jurusan'
    });
    $('.semester_select2').select2({
        placeholder: 'Pilih semester'
    });

    $('.btn-filter').click(function() {
        id_kelas = $('.kelas_select2').val();
        id_pelajaran = $('.pelajaran_select2').val();
        id_tahun_ajaran = $('.tahun_ajaran_select2').val();
        id_jurusan = $('.jurusan_select2').val();
        id_semester = $('.semester_select2').val();
        $('.modal-filter').modal('hide');
        if (id_kelas && id_pelajaran && id_tahun_ajaran && id_jurusan && id_semester) {
            get_data();
        } else {
            alert('Harap isi semua kolom filter');
        }

    });
    $('.trigger-reset-filter').click(function() {
        id_siswa = null;
        id_kelas = null;
        id_pelajaran = null;
        id_tahun_ajaran = null;
        id_jurusan = null;
        id_semester = null;
        get_data();
    });

    function edit(id, nilai) {
        $('.ed_id').val(id);
        $('.ed_nilai').val(nilai);

        $('.modal_ed_nilai').modal('show');
    };

    $('.btn-simpan-perubahan').click(function() {
        $.ajax({
            method: 'POST',
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id: $('.ed_id').val(),
                nilai: $('.ed_nilai').val(),
            },
            cache: false,
            url: "../nilai/api/update/siswa",
            success: function(response) {
                get_data();
                $('.modal_ed_nilai').modal('hide');
            }
        });
    });

    function delete_nilai(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $.ajax({
                method: 'POST',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    id: id,
                },
                cache: false,
                url: "../nilai/api/delete/siswa",
                success: function(response) {
                    get_data();
                }
            });
        }
    };
</script>
@endsection