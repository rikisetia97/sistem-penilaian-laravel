<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&amp;family=Roboto+Mono&amp;display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <link href="{{url('css/ltr-core.css')}}" rel="stylesheet">
    <link href="{{url('css/ltr-vendor.css')}}" rel="stylesheet">
    <link href="{{url('images/favicon.ico')}}" rel="shortcut icon" type="image/x-icon">
    <title>@yield('title')</title>
    @yield('style')
</head>

<body class="theme-light preload-active aside-active aside-mobile-minimized aside-desktop-maximized" id="fullscreen">
    <div class="preload">
        <div class="preload-dialog">
            <div class="spinner-border text-primary preload-spinner"></div>
        </div>
    </div>
    <div class="holder">
        @include('templates/sidebar')
        <div class="wrapper">
            @include('templates/header')
            <div class="content">
                @yield('content')
            </div>
            @include('templates/footer')
        </div>
    </div>
    <div class="modal fade modal-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-alert"></i> Informasi</h5><button type="button" class="btn btn-label-dark btn-icon" data-dismiss="modal"><i class="fa fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <p class="text-alert"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <div class="scrolltop"><button class="btn btn-info btn-icon btn-lg"><i class="fa fa-angle-up"></i></button></div>
    <script type="text/javascript" src="{{url('js/mandatory.js')}}"></script>
    <script type="text/javascript" src="{{url('js/core.js')}}"></script>
    <script type="text/javascript" src="{{url('js/vendor.js')}}"></script>
    <script type="text/javascript" src="{{url('js/context-menu.js')}}"></script>
    @yield('script')
</body>

</html>