<div class="header">
    <div class="header-holder header-holder-desktop sticky-header" id="sticky-header-desktop">
        <div class="header-container container-fluid">
            <div class="header-wrap">
            </div>
            <div class="header-wrap header-wrap-block">
            </div>
            <div class="header-wrap">
                <div class="dropdown ml-2"><button class="btn btn-flat-primary widget13" data-toggle="dropdown">
                        <div class="widget13-text"><strong>{{session()->get('sess_user')['nama']}}</strong></div>
                        <div class="avatar avatar-info widget13-avatar">
                            <div class="avatar-display"><i class="fa fa-user-alt"></i></div>
                        </div>
                    </button>
                    <div class="dropdown-menu dropdown-menu-wide dropdown-menu-right dropdown-menu-animated overflow-hidden py-0">
                        <div class="portlet border-0">
                            <div class="portlet-header bg-primary rounded-0">
                                <div class="rich-list-item w-100 p-0">
                                    <div class="rich-list-prepend">
                                        <div class="avatar">
                                            <div class="avatar-display"><img src="{{url('images/avatar/avatar-1.webp')}}" alt="Avatar image"></div>
                                        </div>
                                    </div>
                                    <div class="rich-list-content">
                                        <h3 class="rich-list-title text-white">{{session()->get('sess_user')['nama']}}</h3><span class="rich-list-subtitle text-white">{{ucwords(session()->get('sess_user')['role'])}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body p-0">
                            </div>
                            <div class="portlet-footer portlet-footer-bordered rounded-0"><a href="{{ url('logout') }}"><button class="btn btn-block btn-label-danger"><i class="fa fa-sign-out-alt"></i> Sign out</button></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('md-breadcrumb')
    <div class="header-holder header-holder-mobile sticky-header" id="sticky-header-mobile">
        <div class="header-container container-fluid">
            <div class="header-wrap"><button class="btn btn-flat-primary btn-icon" data-toggle="aside"><i class="fa fa-bars"></i></button></div>
            <div class="header-wrap header-wrap-block justify-content-start px-3">
                <h4 class="header-brand text-muted"><span class="text-primary">Portal |</span> Sekolah</h4>
            </div>
            <div class="dropdown ml-2"><button class="btn btn-flat-primary widget13" data-toggle="dropdown">
                    <div class="widget13-text text-truncate">{{session()->get('sess_user')['nama']}}</div>
                    <div class="avatar avatar-info widget13-avatar">
                        <div class="avatar-display"><i class="fa fa-user-alt"></i></div>
                    </div>
                </button>
                <div class="dropdown-menu dropdown-menu-wide dropdown-menu-right dropdown-menu-animated overflow-hidden py-0">
                    <div class="portlet border-0">
                        <div class="portlet-header bg-primary rounded-0">
                            <div class="rich-list-item w-100 p-0">
                                <div class="rich-list-prepend">
                                    <div class="avatar avatar-circle">
                                        <div class="avatar-display"><img src="{{url('images/avatar/avatar-4.webp')}}" alt="Avatar image"></div>
                                    </div>
                                </div>
                                <div class="rich-list-content">
                                    <h3 class="rich-list-title text-white">{{session()->get('sess_user')['nama']}}</h3><span class="rich-list-subtitle text-white">Software Engineer</span>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body p-0">
                        </div>
                        <div class="portlet-footer portlet-footer-bordered rounded-0"><a href="{{ url('logout') }}"><button class="btn btn-block btn-label-danger"><i class="fa fa-sign-out-alt"></i> Sign out</button></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('sm-breadcrumb')
</div>