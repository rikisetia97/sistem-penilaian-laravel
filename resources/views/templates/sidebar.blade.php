<div class="aside">
    <div class="aside-header">
        <h3 class="aside-title text-primary">> SMK <span class="aside-title text-muted">NEGERI 1</span></h3>

        <div class="aside-addon">
            <button class="btn btn-label-primary btn-icon btn-lg" data-toggle="aside"><i class="fa fa-times aside-icon-minimize"></i> <i class="fa fa-thumbtack aside-icon-maximize"></i></button>
        </div>

    </div>
    <span style="position: absolute; top: 40px; left: 33px; color: #757575; font-size: 13px;">Karangdadap</span>
    <div class="aside-body mt-5" data-simplebar="data-simplebar">
        @if(session()->get('sess_user')['role'] == 'admin')
        <div class="menu">
            <div class="menu-item"><a href="{{url('/')}}" class="menu-item-link {{check_active_menu('','')}}">
                    <div class="menu-item-icon"><i class="fa fa-tachometer-alt"></i></div><span class="menu-item-text">Dashboard</span>
                    <div class="menu-item-addon"><span class="badge badge-danger"><i class="fa fa-star"></i></span></div>
                </a></div>
            <div class="menu-section">
                <div class="menu-section-icon"><i class="fa fa-ellipsis-h"></i></div>
                <h2 class="menu-section-text">Menu Akademik</h2>
            </div>

            <!-- <div class="menu-item"><a href="{{url('jadwal')}}" class="menu-item-link">
                    <div class="menu-item-icon"><i class="fa fa-calendar"></i></div><span class="menu-item-text">Data Jadwal Pelajaran<span class="float-right badge badge-warning">Soon</span></span>
                    <div class="menu-item-addon"></div>
                </a></div> -->
            <div class="menu-item"><a href="{{url('siswa')}}" class="menu-item-link">
                    <div class="menu-item-icon"><i class="fa fa-users"></i></div><span class="menu-item-text">Data Siswa</span>
                    <div class="menu-item-addon"></div>
                </a></div>
            <div class="menu-item"><a href="{{url('/guru')}}" class="menu-item-link">
                    <div class="menu-item-icon"><i class="fa fa-id-card"></i></div><span class="menu-item-text">Data Guru</span>
                    <div class="menu-item-addon"></div>
                </a></div>
            <div class="menu-item"><a href="{{url('/walikelas')}}" class="menu-item-link">
                    <div class="menu-item-icon"><i class="fa fa-id-card"></i></div><span class="menu-item-text">Data Walikelas</span>
                    <div class="menu-item-addon"></div>
                </a></div>
            <div class="menu-item"><a href="{{url('akademik/kelas')}}" class="menu-item-link {{check_active_menu('akademik','kelas')}}">
                    <div class="menu-item-icon"><i class="fa fa-university"></i></div><span class="menu-item-text">Data Kelas</span>
                    <div class="menu-item-addon"></div>
                </a></div>
            <div class="menu-item"><a href="{{url('akademik/jurusan')}}" class="menu-item-link {{check_active_menu('akademik','jurusan')}}">
                    <div class="menu-item-icon"><i class="fa fa-cogs"></i></div><span class="menu-item-text">Data Jurusan</span>
                    <div class="menu-item-addon"></div>
                </a></div>
            <div class="menu-item"><a href="{{url('akademik/pelajaran')}}" class="menu-item-link {{check_active_menu('akademik','pelajaran')}}">
                    <div class="menu-item-icon"><i class="fa fa-shapes"></i></div><span class="menu-item-text">Data Mata Pelajaran</span>
                    <div class="menu-item-addon"></div>
                </a></div>
            <div class="menu-item"><a href="{{url('akademik/tahun_ajaran')}}" class="menu-item-link {{check_active_menu('akademik','tahun_ajaran')}}">
                    <div class="menu-item-icon"><i class="fa fa-calendar-alt"></i></div><span class="menu-item-text">Data Tahun Ajaran</span>
                    <div class="menu-item-addon"></div>
                </a></div>

            <div class="menu-item"><a href="{{ url('logout') }}" class="menu-item-link mt-4    ">
                    <span class="btn btn-label-danger menu-item-text"><i class="fa fa-sign-out-alt"></i> Sign out</span>
                    <div class="menu-item-addon"></div>
                </a></div>
        </div>
        @elseif(session()->get('sess_user')['role'] == 'guru')
        <div class="menu">
            <div class="menu-item"><a href="{{url('/')}}" class="menu-item-link {{check_active_menu('','')}}">
                    <div class="menu-item-icon"><i class="fa fa-tachometer-alt"></i></div><span class="menu-item-text">Dashboard</span>
                    <div class="menu-item-addon"><span class="badge badge-danger"><i class="fa fa-star"></i></span></div>
                </a></div>
            <div class="menu-section">
                <div class="menu-section-icon"><i class="fa fa-ellipsis-h"></i></div>
                <h2 class="menu-section-text">Menu Guru</h2>
            </div>
            <div class="menu-item"><button class="menu-item-link menu-item-toggle {{check_active_menu('absensi','siswa')}}">
                    <div class="menu-item-icon"><i class="fa fa-laptop"></i></div><span class="menu-item-text">Data Absensi</span>
                    <div class="menu-item-addon"><i class="menu-item-caret caret"></i></div>
                </button>
                <div class="menu-submenu">
                    <div class="menu-item"><a href="{{url('absensi')}}" class="menu-item-link {{check_active_menu('absensi','')}}"><i class="menu-item-bullet"></i> <span class="menu-item-text">Input Absensi Baru</span></a> </div>
                    <div class="menu-item"><a href="{{url('absensi/siswa')}}" class="menu-item-link {{check_active_menu('absensi','siswa')}}"><i class="menu-item-bullet"></i> <span class="menu-item-text">Daftar Absensi</span></a> </div>
                </div>
            </div>
            <div class="menu-item"><button class="menu-item-link menu-item-toggle">
                    <div class="menu-item-icon"><i class="fa fa-chalkboard-teacher"></i></div><span class="menu-item-text">Data Penilaian</span>
                    <div class="menu-item-addon"><i class="menu-item-caret caret"></i></div>
                </button>
                <div class="menu-submenu">
                    <div class="menu-item"><a href="{{url('nilai')}}" class="menu-item-link"><i class="menu-item-bullet"></i> <span class="menu-item-text">Input Nilai Baru</span></a> </div>
                    <div class="menu-item"><a href="{{url('nilai/siswa')}}" class="menu-item-link"><i class="menu-item-bullet"></i> <span class="menu-item-text">Daftar Nilai</span></a> </div>
                </div>
            </div>
            @if(check_walikelas())
            <div class="menu-section">
                <div class="menu-section-icon"><i class="fa fa-ellipsis-h"></i></div>
                <h2 class="menu-section-text">Menu Wali Kelas</h2>
            </div>
            <div class="menu-item"><a href="{{url('/rekap/raport')}}" class="menu-item-link">
                    <div class="menu-item-icon"><i class="fa fa-id-card"></i></div><span class="menu-item-text">Rekap Nilai & Absensi</span>
                    <div class="menu-item-addon"></div>
                </a></div>
            <div class="menu-item"><a href="{{ url('logout') }}" class="menu-item-link mt-4    ">
                    <span class="btn btn-label-danger menu-item-text"><i class="fa fa-sign-out-alt"></i> Sign out</span>
                    <div class="menu-item-addon"></div>
                </a></div>
            @endif
        </div>
        @elseif(session()->get('sess_user')['role'] == 'siswa')
        <div class="menu">
            <div class="menu-item"><a href="{{url('/')}}" class="menu-item-link {{check_active_menu('','')}}">
                    <div class="menu-item-icon"><i class="fa fa-tachometer-alt"></i></div><span class="menu-item-text">Dashboard</span>
                    <div class="menu-item-addon"><span class="badge badge-danger"><i class="fa fa-star"></i></span></div>
                </a></div>
            <div class="menu-section">
                <div class="menu-section-icon"><i class="fa fa-ellipsis-h"></i></div>
                <h2 class="menu-section-text">Menu Siswa</h2>
            </div>
            <div class="menu-item"><a href="{{url('/absensi/persiswa')}}" class="menu-item-link">
                    <div class="menu-item-icon"><i class="fa fa-laptop"></i></div><span class="menu-item-text">Daftar Kehadiran Saya</span>
                    <div class="menu-item-addon"></div>
                </a></div>
            <div class="menu-item"><a href="{{url('/nilai/persiswa')}}" class="menu-item-link">
                    <div class="menu-item-icon"><i class="fa fa-chalkboard-teacher"></i></div><span class="menu-item-text">Daftar Nilai Saya</span>
                    <div class="menu-item-addon"></div>
                </a></div>

            <div class="menu-item"><a href="{{ url('logout') }}" class="menu-item-link mt-4    ">
                    <span class="btn btn-label-danger menu-item-text"><i class="fa fa-sign-out-alt"></i> Sign out</span>
                    <div class="menu-item-addon"></div>
                </a></div>
        </div>
        @endif
    </div>
</div>