<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&amp;family=Roboto+Mono&amp;display=swap" rel="stylesheet">
    <link href="{{url('css/ltr-core.css')}}" rel="stylesheet">
    <link href="{{url('css/ltr-vendor.css')}}" rel="stylesheet">
    <link href="{{url('images/favicon.ico')}}" rel="shortcut icon" type="image/x-icon">
    <title>Selamat Datang | Portal Admin</title>
</head>

<body class="theme-light preload-active" id="fullscreen">
    <div class="preload">
        <div class="preload-dialog">
            <div class="spinner-border text-primary preload-spinner"></div>
        </div>
    </div>
    <div class="holder">
        <div class="wrapper">
            <div class="content">
                <div class="container-fluid">
                    <div class="row no-gutters align-items-center justify-content-center h-100">
                        <div class="col-lg-8 col-xl-6">
                            <div class="portlet overflow-hidden">
                                <div class="row no-gutters">
                                    <div class="col-md-6">
                                        <div class="portlet-body d-flex flex-column justify-content-center align-items-start h-100 bg-primary text-white">
                                            <h5>Selamat Datang di</h5>
                                            <h3 class="text-white mb-2">> Portal | <span class="text-dark">Sekolah</span></h3>
                                            <span>SMKN 1 Karangdadap</span><br>
                                            <i>Portal Sekolah SMKN 1 Karangdadap menyediakan fitur untuk pengolahan nilai siswa, pengolahan jadwal pelajaran, dan absensi siswa dapat dilihat di sistem.</i>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet-body h-100">
                                            <div class="default-info">
                                                <div class="d-flex justify-content-center mb-4">
                                                    <button class="btn btn-label-primary btn-pill" onclick="$('.input-n').val(123001), $('.input-p').val('admin1')">123001 : admin1</button>
                                                    <button class="btn btn-label-info btn-pill mx-2" onclick="$('.input-n').val(123002), $('.input-p').val('guru1')">123002 : guru1</button>
                                                    <button class="btn btn-label-danger btn-pill" onclick="$('.input-n').val(123003), $('.input-p').val('10101998')">123003 : 10101998</button>
                                                </div>
                                            </div>
                                            <div class="show-alert mb-2" style="display: none;">
                                                <div class="alert alert-label-danger alert-text mb-4" role="alert">
                                                    Password salah</div>

                                            </div>
                                            @if ($message = Session::get('success'))
                                            <div class="show-alert-logout mb-1">
                                                <div class="alert alert-label-success" role="alert">
                                                    {{ session('success') }}
                                                </div>
                                            </div>
                                            @endif
                                            <form id="login-form">
                                                <div class="form-group">
                                                    <div class="float-label float-label-lg"><input class="form-control form-control-lg input-n" type="text" placeholder="Mohon masukkan nomor induk anda"> <label for="nisn">Nomor Induk</label></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="float-label float-label-lg"><input class="form-control form-control-lg input-p" type="password" placeholder="Mohon masukkan kata sandi anda"> <label for="password">Kata Sandi</label></div>
                                                </div>
                                                <div class="d-flex align-items-center justify-content-between mb-4">
                                                    <div class="form-group mb-0">
                                                        <div class="custom-control custom-control-lg custom-switch">
                                                            <input type="checkbox" class="custom-control-input" id="remember" name="remember"> <label class="custom-control-label" for="remember">Ingatkan saya</label>
                                                        </div>
                                                    </div>
                                                </div><button type="button" class="btn btn-label-primary btn-lg btn-block btn-pill btn-submit-login">Login</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{url('js/mandatory.js')}}"></script>
    <script type="text/javascript" src="{{url('js/core.js')}}"></script>
    <script type="text/javascript" src="{{url('js/vendor.js')}}"></script>
    <script>
        $('.btn-submit-login').click(function() {
            $('.show-alert-logout').attr('style', 'display:none');
            var dataPost = {
                _token: $('meta[name="csrf-token"]').attr('content'),
                a: $('.input-n').val(),
                b: $('.input-p').val()
            };
            if (dataPost.a.length == 0) {
                $('.input-n').addClass('is-invalid');
                $('.input-p').addClass('is-invalid');
                $('.show-alert').removeAttr('style');
                $('.alert-text').html('Username & password tidak boleh kosong!');
            } else if (dataPost.b.length == 0) {
                $('.input-n').addClass('is-invalid');
                $('.input-p').addClass('is-invalid');
                $('.show-alert').removeAttr('style');
                $('.alert-text').html('Username & password tidak boleh kosong!');
            } else {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "api/check",
                    data: dataPost,
                    success: function(response) {
                        var obj = JSON.parse(response);
                        if (!obj.n) {
                            $('.input-n').addClass('is-invalid');
                            $('.show-alert').removeAttr('style');
                            $('.alert-text').html('Username tidak terdaftar, mohon teliti kembali!');
                        } else if (!obj.p) {
                            $('.input-n').removeClass('is-invalid');
                            $('.input-n').addClass('is-valid');
                            $('.input-p').addClass('is-invalid');
                            $('.show-alert').removeAttr('style');
                            $('.alert-text').html('Password salah, mohon teliti kembali!');
                        } else {
                            location.href = './';
                        }
                        setTimeout(() => {
                            $('.show-alert').attr('style', 'display:none');
                            $('.input-n').removeClass('is-invalid');
                            $('.input-n').removeClass('is-valid');
                            $('.input-p').removeClass('is-invalid');
                            $('.input-p').removeClass('is-valid');
                        }, 10000);
                    }
                });
            }

        })
    </script>
</body>

</html>